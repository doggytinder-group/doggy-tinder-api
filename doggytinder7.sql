-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for animals
DROP DATABASE IF EXISTS `animals`;
CREATE DATABASE IF NOT EXISTS `animals` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `animals`;

-- Dumping structure for table animals.breed_type
DROP TABLE IF EXISTS `breed_type`;
CREATE TABLE IF NOT EXISTS `breed_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pet_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pet_type_id` (`pet_type_id`),
  CONSTRAINT `FK_pet_type_id` FOREIGN KEY (`pet_type_id`) REFERENCES `pet_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.breed_type: ~346 rows (approximately)
/*!40000 ALTER TABLE `breed_type` DISABLE KEYS */;
INSERT INTO `breed_type` (`id`, `name`, `section`, `country`, `url`, `image`, `pdf`, `pet_type_id`) VALUES
	(1, 'English Pointer', 'British and Irish Pointers and Setters', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-POINTER-1.html', 'http://www.fci.be/Nomenclature/Illustrations/001g07.jpg', 'http://www.fci.be/Nomenclature/Standards/001g07-en.pdf\r', NULL),
	(2, 'English Setter', 'British and Irish Pointers and Setters', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-SETTER-2.html', 'http://www.fci.be/Nomenclature/Illustrations/002g07.jpg', 'http://www.fci.be/Nomenclature/Standards/002g07-en.pdf\r', NULL),
	(3, 'Kerry Blue Terrier', 'Large and medium sized Terriers', 'Ireland', 'http://www.fci.be/en/nomenclature/KERRY-BLUE-TERRIER-3.html', '', 'http://www.fci.be/Nomenclature/Standards/003g03-en.pdf\r', NULL),
	(4, 'Cairn Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/CAIRN-TERRIER-4.html', 'http://www.fci.be/Nomenclature/Illustrations/004g03.jpg', 'http://www.fci.be/Nomenclature/Standards/004g03-en.pdf\r', NULL),
	(5, 'English Cocker Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-COCKER-SPANIEL-5.html', 'http://www.fci.be/Nomenclature/Illustrations/005g08.jpg', 'http://www.fci.be/Nomenclature/Standards/005g08-en.pdf\r', NULL),
	(6, 'Gordon Setter', 'British and Irish Pointers and Setters', 'Great Britain', 'http://www.fci.be/en/nomenclature/GORDON-SETTER-6.html', 'http://www.fci.be/Nomenclature/Illustrations/006g07.jpg', 'http://www.fci.be/Nomenclature/Standards/006g07-en.pdf\r', NULL),
	(7, 'Airedale Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/AIREDALE-TERRIER-7.html', 'http://www.fci.be/Nomenclature/Illustrations/007g03.jpg', 'http://www.fci.be/Nomenclature/Standards/007g03-en.pdf\r', NULL),
	(8, 'Australian Terrier', 'Small sized Terriers', 'Australia', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-TERRIER-8.html', 'http://www.fci.be/Nomenclature/Illustrations/008g03.jpg', 'http://www.fci.be/Nomenclature/Standards/008g03-en.pdf\r', NULL),
	(9, 'Bedlington Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/BEDLINGTON-TERRIER-9.html', 'http://www.fci.be/Nomenclature/Illustrations/009g03.jpg', 'http://www.fci.be/Nomenclature/Standards/009g03-en.pdf\r', NULL),
	(10, 'Border Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/BORDER-TERRIER-10.html', '', 'http://www.fci.be/Nomenclature/Standards/010g03-en.pdf\r', NULL),
	(11, 'Bull Terrier', 'Bull type Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/BULL-TERRIER-11.html', 'http://www.fci.be/Nomenclature/Illustrations/011g03.jpg', 'http://www.fci.be/Nomenclature/Standards/011g03-en.pdf\r', NULL),
	(12, 'Fox Terrier (smooth)', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/FOX-TERRIER-SMOOTH-12.html', 'http://www.fci.be/Nomenclature/Illustrations/012g03.jpg', 'http://www.fci.be/Nomenclature/Standards/012g03-en.pdf\r', NULL),
	(13, 'English Toy Terrier (black & tan)', 'Toy Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-TOY-TERRIER-BLACK-TAN-13.html', 'http://www.fci.be/Nomenclature/Illustrations/013g03.jpg', 'http://www.fci.be/Nomenclature/Standards/013g03-en.pdf\r', NULL),
	(14, 'Swedish Vallhund', 'Nordic Watchdogs and Herders', 'Sweden', 'http://www.fci.be/en/nomenclature/SWEDISH-VALLHUND-14.html', 'http://www.fci.be/Nomenclature/Illustrations/014g05.jpg', 'http://www.fci.be/Nomenclature/Standards/014g05-en.pdf\r', NULL),
	(15, 'Belgian Shepherd Dog', 'Sheepdogs', 'Belgium', 'http://www.fci.be/en/nomenclature/BELGIAN-SHEPHERD-DOG-15.html', '', 'http://www.fci.be/Nomenclature/Standards/015g01-en.pdf\r', NULL),
	(16, 'Old English Sheepdog', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/OLD-ENGLISH-SHEEPDOG-16.html', 'http://www.fci.be/Nomenclature/Illustrations/016g01.jpg', 'http://www.fci.be/Nomenclature/Standards/016g01-en.pdf\r', NULL),
	(17, 'Griffon Nivernais', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GRIFFON-NIVERNAIS-17.html', 'http://www.fci.be/Nomenclature/Illustrations/017g06.jpg', 'http://www.fci.be/Nomenclature/Standards/017g06-en.pdf\r', NULL),
	(19, 'Briquet Griffon Vendeen', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BRIQUET-GRIFFON-VENDEEN-19.html', 'http://www.fci.be/Nomenclature/Illustrations/019g06.jpg', 'http://www.fci.be/Nomenclature/Standards/019g06-en.pdf\r', NULL),
	(20, 'Ariegeois', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/ARIEGEOIS-20.html', 'http://www.fci.be/Nomenclature/Illustrations/020g06.jpg', 'http://www.fci.be/Nomenclature/Standards/020g06-en.pdf\r', NULL),
	(21, 'Gascon Saintongeois', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GASCON-SAINTONGEOIS-21.html', '', 'http://www.fci.be/Nomenclature/Standards/021g06-en.pdf\r', NULL),
	(22, 'Great Gascony Blue', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GREAT-GASCONY-BLUE-22.html', 'http://www.fci.be/Nomenclature/Illustrations/022g06.jpg', 'http://www.fci.be/Nomenclature/Standards/022g06-en.pdf\r', NULL),
	(24, 'Poitevin', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/POITEVIN-24.html', 'http://www.fci.be/Nomenclature/Illustrations/024g06.jpg', 'http://www.fci.be/Nomenclature/Standards/024g06-en.pdf\r', NULL),
	(25, 'Billy', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BILLY-25.html', 'http://www.fci.be/Nomenclature/Illustrations/025g06.jpg', 'http://www.fci.be/Nomenclature/Standards/025g06-en.pdf\r', NULL),
	(28, 'Artois Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/ARTOIS-HOUND-28.html', 'http://www.fci.be/Nomenclature/Illustrations/028g06.jpg', 'http://www.fci.be/Nomenclature/Standards/028g06-en.pdf\r', NULL),
	(30, 'Porcelaine', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/PORCELAINE-30.html', 'http://www.fci.be/Nomenclature/Illustrations/030g06.jpg', 'http://www.fci.be/Nomenclature/Standards/030g06-en.pdf\r', NULL),
	(31, 'Small Blue Gascony', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/SMALL-BLUE-GASCONY-31.html', '', 'http://www.fci.be/Nomenclature/Standards/031g06-en.pdf\r', NULL),
	(32, 'Blue Gascony Griffon', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BLUE-GASCONY-GRIFFON-32.html', '', 'http://www.fci.be/Nomenclature/Standards/032g06-en.pdf\r', NULL),
	(33, 'Grand Basset Griffon Vendeen', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GRAND-BASSET-GRIFFON-VENDEEN-33.html', '', 'http://www.fci.be/Nomenclature/Standards/033g06-en.pdf\r', NULL),
	(34, 'Norman Artesien Basset', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/NORMAN-ARTESIEN-BASSET-34.html', '', 'http://www.fci.be/Nomenclature/Standards/034g06-en.pdf\r', NULL),
	(35, 'Blue Gascony Basset', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BLUE-GASCONY-BASSET-35.html', '', 'http://www.fci.be/Nomenclature/Standards/035g06-en.pdf\r', NULL),
	(36, 'Basset Fauve De Bretagne', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BASSET-FAUVE-DE-BRETAGNE-36.html', '', 'http://www.fci.be/Nomenclature/Standards/036g06-en.pdf\r', NULL),
	(37, 'Portuguese Water Dog', 'Water Dogs', 'Portugal', 'http://www.fci.be/en/nomenclature/PORTUGUESE-WATER-DOG-37.html', 'http://www.fci.be/Nomenclature/Illustrations/037g08.jpg', 'http://www.fci.be/Nomenclature/Standards/037g08-en.pdf\r', NULL),
	(38, 'Welsh Corgi (cardigan)', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/WELSH-CORGI-CARDIGAN-38.html', 'http://www.fci.be/Nomenclature/Illustrations/038g01.jpg', 'http://www.fci.be/Nomenclature/Standards/038g01-en.pdf\r', NULL),
	(39, 'Welsh Corgi (pembroke)', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/WELSH-CORGI-PEMBROKE-39.html', 'http://www.fci.be/Nomenclature/Illustrations/039g01.jpg', 'http://www.fci.be/Nomenclature/Standards/039g01-en.pdf\r', NULL),
	(40, 'Irish Soft Coated Wheaten Terrier', 'Large and medium sized Terriers', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-SOFT-COATED-WHEATEN-TERRIER-40.html', '', 'http://www.fci.be/Nomenclature/Standards/040g03-en.pdf\r', NULL),
	(41, 'Yugoslavian Shepherd Dog - Sharplanina', 'Molossian type', 'Macedonia, Serbia', 'http://www.fci.be/en/nomenclature/YUGOSLAVIAN-SHEPHERD-DOG-SHARPLANINA-41.html', '', 'http://www.fci.be/Nomenclature/Standards/041g02-en.pdf\r', NULL),
	(42, 'Jämthund', 'Nordic Hunting Dogs', 'Sweden', 'http://www.fci.be/en/nomenclature/JAMTHUND-42.html', 'http://www.fci.be/Nomenclature/Illustrations/042g05.jpg', 'http://www.fci.be/Nomenclature/Standards/042g05-en.pdf\r', NULL),
	(43, 'Basenji', 'Primitive type', 'Central Africa', 'http://www.fci.be/en/nomenclature/BASENJI-43.html', '', 'http://www.fci.be/Nomenclature/Standards/043g05-en.pdf\r', NULL),
	(44, 'Berger De Beauce', 'Sheepdogs', 'France', 'http://www.fci.be/en/nomenclature/BERGER-DE-BEAUCE-44.html', '', 'http://www.fci.be/Nomenclature/Standards/044g01-en.pdf\r', NULL),
	(45, 'Bernese Mountain Dog', 'Swiss Mountain- and Cattledogs', 'Switzerland', 'http://www.fci.be/en/nomenclature/BERNESE-MOUNTAIN-DOG-45.html', 'http://www.fci.be/Nomenclature/Illustrations/045g02.jpg', 'http://www.fci.be/Nomenclature/Standards/045g02-en.pdf\r', NULL),
	(46, 'Appenzell Cattle Dog', 'Swiss Mountain- and Cattledogs', 'Switzerland', 'http://www.fci.be/en/nomenclature/APPENZELL-CATTLE-DOG-46.html', '', 'http://www.fci.be/Nomenclature/Standards/046g02-en.pdf\r', NULL),
	(47, 'Entlebuch Cattle Dog', 'Swiss Mountain- and Cattledogs', 'Switzerland', 'http://www.fci.be/en/nomenclature/ENTLEBUCH-CATTLE-DOG-47.html', 'http://www.fci.be/Nomenclature/Illustrations/047g02.jpg', 'http://www.fci.be/Nomenclature/Standards/047g02-en.pdf\r', NULL),
	(48, 'Karelian Bear Dog', 'Nordic Hunting Dogs', 'Finland', 'http://www.fci.be/en/nomenclature/KARELIAN-BEAR-DOG-48.html', 'http://www.fci.be/Nomenclature/Illustrations/048g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/048g05-en.pdf\r', NULL),
	(49, 'Finnish Spitz', 'Nordic Hunting Dogs', 'Finland', 'http://www.fci.be/en/nomenclature/FINNISH-SPITZ-49.html', 'http://www.fci.be/Nomenclature/Illustrations/049g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/049g05-en.pdf\r', NULL),
	(50, 'Newfoundland', 'Molossian type', 'Canada', 'http://www.fci.be/en/nomenclature/NEWFOUNDLAND-50.html', '', 'http://www.fci.be/Nomenclature/Standards/050g02-en.pdf\r', NULL),
	(51, 'Finnish Hound', 'Scent hounds', 'Finland', 'http://www.fci.be/en/nomenclature/FINNISH-HOUND-51.html', '', 'http://www.fci.be/Nomenclature/Standards/051g06-en.pdf\r', NULL),
	(52, 'Polish Hound', 'Scent hounds', 'Poland', 'http://www.fci.be/en/nomenclature/POLISH-HOUND-52.html', 'http://www.fci.be/Nomenclature/Illustrations/052g06.jpg', 'http://www.fci.be/Nomenclature/Standards/052g06-en.pdf\r', NULL),
	(53, 'Komondor', 'Sheepdogs', 'Hungary', 'http://www.fci.be/en/nomenclature/KOMONDOR-53.html', 'http://www.fci.be/Nomenclature/Illustrations/053g01.jpg', 'http://www.fci.be/Nomenclature/Standards/053g01-en.pdf\r', NULL),
	(54, 'Kuvasz', 'Sheepdogs', 'Hungary', 'http://www.fci.be/en/nomenclature/KUVASZ-54.html', 'http://www.fci.be/Nomenclature/Illustrations/054g01.jpg', 'http://www.fci.be/Nomenclature/Standards/054g01-en.pdf\r', NULL),
	(55, 'Puli', 'Sheepdogs', 'Hungary', 'http://www.fci.be/en/nomenclature/PULI-55.html', 'http://www.fci.be/Nomenclature/Illustrations/055g01.jpg', 'http://www.fci.be/Nomenclature/Standards/055g01-en.pdf\r', NULL),
	(56, 'Pumi', 'Sheepdogs', 'Hungary', 'http://www.fci.be/en/nomenclature/PUMI-56.html', 'http://www.fci.be/Nomenclature/Illustrations/056g01.jpg', 'http://www.fci.be/Nomenclature/Standards/056g01-en.pdf\r', NULL),
	(57, 'Hungarian Short-haired Pointer (vizsla)', 'Continental Pointing Dogs', 'Hungary', 'http://www.fci.be/en/nomenclature/HUNGARIAN-SHORT-HAIRED-POINTER-VIZSLA-57.html', 'http://www.fci.be/Nomenclature/Illustrations/057g07.jpg', 'http://www.fci.be/Nomenclature/Standards/057g07-en.pdf\r', NULL),
	(58, 'Great Swiss Mountain Dog', 'Swiss Mountain- and Cattledogs', 'Switzerland', 'http://www.fci.be/en/nomenclature/GREAT-SWISS-MOUNTAIN-DOG-58.html', 'http://www.fci.be/Nomenclature/Illustrations/058g02.jpg', 'http://www.fci.be/Nomenclature/Standards/058g02-en.pdf\r', NULL),
	(59, 'Swiss Hound', 'Scent hounds', 'Switzerland', 'http://www.fci.be/en/nomenclature/SWISS-HOUND-59.html', 'http://www.fci.be/Nomenclature/Illustrations/059g06-1.jpg', 'http://www.fci.be/Nomenclature/Standards/059g06-en.pdf\r', NULL),
	(60, 'Small Swiss Hound', 'Scent hounds', 'Switzerland', 'http://www.fci.be/en/nomenclature/SMALL-SWISS-HOUND-60.html', 'http://www.fci.be/Nomenclature/Illustrations/060g06-1.jpg', 'http://www.fci.be/Nomenclature/Standards/060g06-en.pdf\r', NULL),
	(61, 'St. Bernard', 'Molossian type', 'Switzerland', 'http://www.fci.be/en/nomenclature/ST-BERNARD-61.html', 'http://www.fci.be/Nomenclature/Illustrations/061g02-1.jpg', 'http://www.fci.be/Nomenclature/Standards/061g02-en.pdf\r', NULL),
	(62, 'Coarse-haired Styrian Hound', 'Scent hounds', 'Austria', 'http://www.fci.be/en/nomenclature/COARSE-HAIRED-STYRIAN-HOUND-62.html', 'http://www.fci.be/Nomenclature/Illustrations/062g06.jpg', 'http://www.fci.be/Nomenclature/Standards/062g06-en.pdf\r', NULL),
	(63, 'Austrian Black And Tan Hound', 'Scent hounds', 'Austria', 'http://www.fci.be/en/nomenclature/AUSTRIAN-BLACK-AND-TAN-HOUND-63.html', 'http://www.fci.be/Nomenclature/Illustrations/063g06.jpg', 'http://www.fci.be/Nomenclature/Standards/063g06-en.pdf\r', NULL),
	(64, 'Austrian Pinscher', 'Pinscher and Schnauzer type', 'Austria', 'http://www.fci.be/en/nomenclature/AUSTRIAN-PINSCHER-64.html', '', 'http://www.fci.be/Nomenclature/Standards/064g02-en.pdf\r', NULL),
	(65, 'Maltese', 'Bichons and related breeds', 'Central Mediterranean Basin', 'http://www.fci.be/en/nomenclature/MALTESE-65.html', '', 'http://www.fci.be/Nomenclature/Standards/065g09-en.pdf\r', NULL),
	(66, 'Fawn Brittany Griffon', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/FAWN-BRITTANY-GRIFFON-66.html', 'http://www.fci.be/Nomenclature/Illustrations/066g06.jpg', 'http://www.fci.be/Nomenclature/Standards/066g06-en.pdf\r', NULL),
	(67, 'Petit Basset Griffon Vendeen', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/PETIT-BASSET-GRIFFON-VENDEEN-67.html', '', 'http://www.fci.be/Nomenclature/Standards/067g06-en.pdf\r', NULL),
	(68, 'Tyrolean Hound', 'Scent hounds', 'Austria', 'http://www.fci.be/en/nomenclature/TYROLEAN-HOUND-68.html', '', 'http://www.fci.be/Nomenclature/Standards/068g06-en.pdf\r', NULL),
	(70, 'Lakeland Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/LAKELAND-TERRIER-70.html', 'http://www.fci.be/Nomenclature/Illustrations/070g03.jpg', 'http://www.fci.be/Nomenclature/Standards/070g03-en.pdf\r', NULL),
	(71, 'Manchester Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/MANCHESTER-TERRIER-71.html', 'http://www.fci.be/Nomenclature/Illustrations/071g03.jpg', 'http://www.fci.be/Nomenclature/Standards/071g03-en.pdf\r', NULL),
	(72, 'Norwich Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/NORWICH-TERRIER-72.html', 'http://www.fci.be/Nomenclature/Illustrations/072g03.jpg', 'http://www.fci.be/Nomenclature/Standards/072g03-en.pdf\r', NULL),
	(73, 'Scottish Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/SCOTTISH-TERRIER-73.html', 'http://www.fci.be/Nomenclature/Illustrations/073g03.jpg', 'http://www.fci.be/Nomenclature/Standards/073g03-en.pdf\r', NULL),
	(74, 'Sealyham Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/SEALYHAM-TERRIER-74.html', 'http://www.fci.be/Nomenclature/Illustrations/074g03.jpg', 'http://www.fci.be/Nomenclature/Standards/074g03-en.pdf\r', NULL),
	(75, 'Skye Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/SKYE-TERRIER-75.html', 'http://www.fci.be/Nomenclature/Illustrations/075g03.jpg', 'http://www.fci.be/Nomenclature/Standards/075g03-en.pdf\r', NULL),
	(76, 'Staffordshire Bull Terrier', 'Bull type Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/STAFFORDSHIRE-BULL-TERRIER-76.html', 'http://www.fci.be/Nomenclature/Illustrations/076g03.jpg', 'http://www.fci.be/Nomenclature/Standards/076g03-en.pdf\r', NULL),
	(77, 'Continental Toy Spaniel', 'Continental Toy Spaniel and Russian Toy', 'Belgium, France', 'http://www.fci.be/en/nomenclature/CONTINENTAL-TOY-SPANIEL-77.html', '', 'http://www.fci.be/Nomenclature/Standards/077g09-en.pdf\r', NULL),
	(78, 'Welsh Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/WELSH-TERRIER-78.html', 'http://www.fci.be/Nomenclature/Illustrations/078g03.jpg', 'http://www.fci.be/Nomenclature/Standards/078g03-en.pdf\r', NULL),
	(80, 'Griffon Bruxellois', 'Small Belgian Dogs', 'Belgium', 'http://www.fci.be/en/nomenclature/GRIFFON-BRUXELLOIS-80.html', 'http://www.fci.be/Nomenclature/Illustrations/080g09-1.jpg', 'http://www.fci.be/Nomenclature/Standards/080g09-en.pdf\r', NULL),
	(81, 'Griffon Belge', 'Small Belgian Dogs', 'Belgium', 'http://www.fci.be/en/nomenclature/GRIFFON-BELGE-81.html', '', 'http://www.fci.be/Nomenclature/Standards/081g09-en.pdf\r', NULL),
	(82, 'Petit Brabançon', 'Small Belgian Dogs', 'Belgium', 'http://www.fci.be/en/nomenclature/PETIT-BRABANCON-82.html', '', 'http://www.fci.be/Nomenclature/Standards/082g09-en.pdf\r', NULL),
	(83, 'Schipperke', 'Sheepdogs', 'Belgium', 'http://www.fci.be/en/nomenclature/SCHIPPERKE-83.html', '', 'http://www.fci.be/Nomenclature/Standards/083g01-en.pdf\r', NULL),
	(84, 'Bloodhound', 'Scent hounds', 'Belgium', 'http://www.fci.be/en/nomenclature/BLOODHOUND-84.html', '', 'http://www.fci.be/Nomenclature/Standards/084g06-en.pdf\r', NULL),
	(85, 'West Highland White Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/WEST-HIGHLAND-WHITE-TERRIER-85.html', 'http://www.fci.be/Nomenclature/Illustrations/085g03.jpg', 'http://www.fci.be/Nomenclature/Standards/085g03-en.pdf\r', NULL),
	(86, 'Yorkshire Terrier', 'Toy Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/YORKSHIRE-TERRIER-86.html', 'http://www.fci.be/Nomenclature/Illustrations/086g03.jpg', 'http://www.fci.be/Nomenclature/Standards/086g03-en.pdf\r', NULL),
	(87, 'Catalan Sheepdog', 'Sheepdogs', 'Spain', 'http://www.fci.be/en/nomenclature/CATALAN-SHEEPDOG-87.html', '', 'http://www.fci.be/Nomenclature/Standards/087g01-en.pdf\r', NULL),
	(88, 'Shetland Sheepdog', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/SHETLAND-SHEEPDOG-88.html', 'http://www.fci.be/Nomenclature/Illustrations/088g01.jpg', 'http://www.fci.be/Nomenclature/Standards/088g01-en.pdf\r', NULL),
	(89, 'Ibizan Podenco', 'Primitive type - Hunting Dogs', 'Spain', 'http://www.fci.be/en/nomenclature/IBIZAN-PODENCO-89.html', '', 'http://www.fci.be/Nomenclature/Standards/089g05-en.pdf\r', NULL),
	(90, 'Burgos Pointing Dog', 'Continental Pointing Dogs', 'Spain', 'http://www.fci.be/en/nomenclature/BURGOS-POINTING-DOG-90.html', '', 'http://www.fci.be/Nomenclature/Standards/090g07-en.pdf\r', NULL),
	(91, 'Spanish Mastiff', 'Molossian type', 'Spain', 'http://www.fci.be/en/nomenclature/SPANISH-MASTIFF-91.html', '', 'http://www.fci.be/Nomenclature/Standards/091g02-en.pdf\r', NULL),
	(92, 'Pyrenean Mastiff', 'Molossian type', 'Spain', 'http://www.fci.be/en/nomenclature/PYRENEAN-MASTIFF-92.html', '', 'http://www.fci.be/Nomenclature/Standards/092g02-en.pdf\r', NULL),
	(93, 'Portuguese Sheepdog', 'Sheepdogs', 'Portugal', 'http://www.fci.be/en/nomenclature/PORTUGUESE-SHEEPDOG-93.html', 'http://www.fci.be/Nomenclature/Illustrations/093g01.jpg', 'http://www.fci.be/Nomenclature/Standards/093g01-en.pdf\r', NULL),
	(94, 'Portuguese Warren Hound-portuguese Podengo', 'Primitive type - Hunting Dogs', 'Portugal', 'http://www.fci.be/en/nomenclature/PORTUGUESE-WARREN-HOUND-PORTUGUESE-PODENGO-94.html', 'http://www.fci.be/Nomenclature/Illustrations/094g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/094g05-en.pdf\r', NULL),
	(95, 'Brittany Spaniel', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/BRITTANY-SPANIEL-95.html', 'http://www.fci.be/Nomenclature/Illustrations/095g07.jpg', 'http://www.fci.be/Nomenclature/Standards/095g07-en.pdf\r', NULL),
	(96, 'Rafeiro Of Alentejo', 'Molossian type', 'Portugal', 'http://www.fci.be/en/nomenclature/RAFEIRO-OF-ALENTEJO-96.html', 'http://www.fci.be/Nomenclature/Illustrations/096g02.jpg', 'http://www.fci.be/Nomenclature/Standards/096g02-en.pdf\r', NULL),
	(97, 'German Spitz', 'European Spitz', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-SPITZ-97.html', 'http://www.fci.be/Nomenclature/Illustrations/097g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/097g05-en.pdf\r', NULL),
	(98, 'German Wire-haired Pointing Dog', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-WIRE-HAIRED-POINTING-DOG-98.html', 'http://www.fci.be/Nomenclature/Illustrations/098g07.jpg', 'http://www.fci.be/Nomenclature/Standards/098g07-en.pdf\r', NULL),
	(99, 'Weimaraner', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/WEIMARANER-99.html', '', 'http://www.fci.be/Nomenclature/Standards/099g07-en.pdf\r', NULL),
	(100, 'Westphalian Dachsbracke', 'Scent hounds', 'Germany', 'http://www.fci.be/en/nomenclature/WESTPHALIAN-DACHSBRACKE-100.html', '', 'http://www.fci.be/Nomenclature/Standards/100g06-en.pdf\r', NULL),
	(101, 'French Bulldog', 'Small Molossian type Dogs', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-BULLDOG-101.html', 'http://www.fci.be/Nomenclature/Illustrations/101g09.jpg', 'http://www.fci.be/Nomenclature/Standards/101g09-en.pdf\r', NULL),
	(102, 'Kleiner Münsterländer', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/KLEINER-MUNSTERLANDER-102.html', '', 'http://www.fci.be/Nomenclature/Standards/102g07-en.pdf\r', NULL),
	(103, 'German Hunting Terrier', 'Large and medium sized Terriers', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-HUNTING-TERRIER-103.html', 'http://www.fci.be/Nomenclature/Illustrations/103g03-1.jpg', 'http://www.fci.be/Nomenclature/Standards/103g03-en.pdf\r', NULL),
	(104, 'German Spaniel', 'Flushing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-SPANIEL-104.html', 'http://www.fci.be/Nomenclature/Illustrations/104g08.jpg', 'http://www.fci.be/Nomenclature/Standards/104g08-en.pdf\r', NULL),
	(105, 'French Water Dog', 'Water Dogs', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-WATER-DOG-105.html', 'http://www.fci.be/Nomenclature/Illustrations/105g08.jpg', 'http://www.fci.be/Nomenclature/Standards/105g08-en.pdf\r', NULL),
	(106, 'Blue Picardy Spaniel', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/BLUE-PICARDY-SPANIEL-106.html', '', 'http://www.fci.be/Nomenclature/Standards/106g07-en.pdf\r', NULL),
	(107, 'Wire-haired Pointing Griffon Korthals', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/WIRE-HAIRED-POINTING-GRIFFON-KORTHALS-107.html', '', 'http://www.fci.be/Nomenclature/Standards/107g07-en.pdf\r', NULL),
	(108, 'Picardy Spaniel', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/PICARDY-SPANIEL-108.html', '', 'http://www.fci.be/Nomenclature/Standards/108g07-en.pdf\r', NULL),
	(109, 'Clumber Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/CLUMBER-SPANIEL-109.html', 'http://www.fci.be/Nomenclature/Illustrations/109g08.jpg', 'http://www.fci.be/Nomenclature/Standards/109g08-en.pdf\r', NULL),
	(110, 'Curly Coated Retriever', 'Retrievers', 'Great Britain', 'http://www.fci.be/en/nomenclature/CURLY-COATED-RETRIEVER-110.html', 'http://www.fci.be/Nomenclature/Illustrations/110g08.jpg', 'http://www.fci.be/Nomenclature/Standards/110g08-en.pdf\r', NULL),
	(111, 'Golden Retriever', 'Retrievers', 'Great Britain', 'http://www.fci.be/en/nomenclature/GOLDEN-RETRIEVER-111.html', '', 'http://www.fci.be/Nomenclature/Standards/111g08-en.pdf\r', NULL),
	(113, 'Briard', 'Sheepdogs', 'France', 'http://www.fci.be/en/nomenclature/BRIARD-113.html', 'http://www.fci.be/Nomenclature/Illustrations/113g01.jpg', 'http://www.fci.be/Nomenclature/Standards/113g01-en.pdf\r', NULL),
	(114, 'Pont-audemer Spaniel', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/PONT-AUDEMER-SPANIEL-114.html', '', 'http://www.fci.be/Nomenclature/Standards/114g07-en.pdf\r', NULL),
	(115, 'Saint Germain Pointer', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/SAINT-GERMAIN-POINTER-115.html', '', 'http://www.fci.be/Nomenclature/Standards/115g07-en.pdf\r', NULL),
	(116, 'Dogue De Bordeaux', 'Molossian type', 'France', 'http://www.fci.be/en/nomenclature/DOGUE-DE-BORDEAUX-116.html', 'http://www.fci.be/Nomenclature/Illustrations/116g02.jpg', 'http://www.fci.be/Nomenclature/Standards/116g02-en.pdf\r', NULL),
	(117, 'Deutsch Langhaar', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/DEUTSCH-LANGHAAR-117.html', 'http://www.fci.be/Nomenclature/Illustrations/117g07.jpg', 'http://www.fci.be/Nomenclature/Standards/117g07-en.pdf\r', NULL),
	(118, 'Large Munsterlander', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/LARGE-MUNSTERLANDER-118.html', 'http://www.fci.be/Nomenclature/Illustrations/118g07.jpg', 'http://www.fci.be/Nomenclature/Standards/118g07-en.pdf\r', NULL),
	(119, 'German Short-haired Pointing Dog', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-SHORT-HAIRED-POINTING-DOG-119.html', 'http://www.fci.be/Nomenclature/Illustrations/119g07.jpg', 'http://www.fci.be/Nomenclature/Standards/119g07-en.pdf\r', NULL),
	(120, 'Irish Red Setter', 'British and Irish Pointers and Setters', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-RED-SETTER-120.html', '', 'http://www.fci.be/Nomenclature/Standards/120g07-en.pdf\r', NULL),
	(121, 'Flat Coated Retriever', 'Retrievers', 'Great Britain', 'http://www.fci.be/en/nomenclature/FLAT-COATED-RETRIEVER-121.html', 'http://www.fci.be/Nomenclature/Illustrations/121g08.jpg', 'http://www.fci.be/Nomenclature/Standards/121g08-en.pdf\r', NULL),
	(122, 'Labrador Retriever', 'Retrievers', 'Great Britain', 'http://www.fci.be/en/nomenclature/LABRADOR-RETRIEVER-122.html', 'http://www.fci.be/Nomenclature/Illustrations/122g08.jpg', 'http://www.fci.be/Nomenclature/Standards/122g08-en.pdf\r', NULL),
	(123, 'Field Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/FIELD-SPANIEL-123.html', 'http://www.fci.be/Nomenclature/Illustrations/123g08.jpg', 'http://www.fci.be/Nomenclature/Standards/123g08-en.pdf\r', NULL),
	(124, 'Irish Water Spaniel', 'Water Dogs', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-WATER-SPANIEL-124.html', '', 'http://www.fci.be/Nomenclature/Standards/124g08-en.pdf\r', NULL),
	(125, 'English Springer Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-SPRINGER-SPANIEL-125.html', 'http://www.fci.be/Nomenclature/Illustrations/125g08.jpg', 'http://www.fci.be/Nomenclature/Standards/125g08-en.pdf\r', NULL),
	(126, 'Welsh Springer Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/WELSH-SPRINGER-SPANIEL-126.html', 'http://www.fci.be/Nomenclature/Illustrations/126g08.jpg', 'http://www.fci.be/Nomenclature/Standards/126g08-en.pdf\r', NULL),
	(127, 'Sussex Spaniel', 'Flushing Dogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/SUSSEX-SPANIEL-127.html', 'http://www.fci.be/Nomenclature/Illustrations/127g08.jpg', 'http://www.fci.be/Nomenclature/Standards/127g08-en.pdf\r', NULL),
	(128, 'King Charles Spaniel', 'English Toy Spaniels', 'Great Britain', 'http://www.fci.be/en/nomenclature/KING-CHARLES-SPANIEL-128.html', 'http://www.fci.be/Nomenclature/Illustrations/128g09.jpg', 'http://www.fci.be/Nomenclature/Standards/128g09-en.pdf\r', NULL),
	(129, 'Smålandsstövare', 'Scent hounds', 'Sweden', 'http://www.fci.be/en/nomenclature/SMALANDSSTOVARE-129.html', 'http://www.fci.be/Nomenclature/Illustrations/129g06.jpg', 'http://www.fci.be/Nomenclature/Standards/129g06-en.pdf\r', NULL),
	(130, 'Drever', 'Scent hounds', 'Sweden', 'http://www.fci.be/en/nomenclature/DREVER-130.html', 'http://www.fci.be/Nomenclature/Illustrations/130g06.jpg', 'http://www.fci.be/Nomenclature/Standards/130g06-en.pdf\r', NULL),
	(131, 'Schillerstövare', 'Scent hounds', 'Sweden', 'http://www.fci.be/en/nomenclature/SCHILLERSTOVARE-131.html', 'http://www.fci.be/Nomenclature/Illustrations/131g06.jpg', 'http://www.fci.be/Nomenclature/Standards/131g06-en.pdf\r', NULL),
	(132, 'Hamiltonstövare', 'Scent hounds', 'Sweden', 'http://www.fci.be/en/nomenclature/HAMILTONSTOVARE-132.html', 'http://www.fci.be/Nomenclature/Illustrations/132g06.jpg', 'http://www.fci.be/Nomenclature/Standards/132g06-en.pdf\r', NULL),
	(133, 'French Pointing Dog - Gascogne Type', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-POINTING-DOG-GASCOGNE-TYPE-133.html', '', 'http://www.fci.be/Nomenclature/Standards/133g07-en.pdf\r', NULL),
	(134, 'French Pointing Dog - Pyrenean Type', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-POINTING-DOG-PYRENEAN-TYPE-134.html', '', 'http://www.fci.be/Nomenclature/Standards/134g07-en.pdf\r', NULL),
	(135, 'Swedish Lapphund', 'Nordic Watchdogs and Herders', 'Sweden', 'http://www.fci.be/en/nomenclature/SWEDISH-LAPPHUND-135.html', '', 'http://www.fci.be/Nomenclature/Standards/135g05-en.pdf\r', NULL),
	(136, 'Cavalier King Charles Spaniel', 'English Toy Spaniels', 'Great Britain', 'http://www.fci.be/en/nomenclature/CAVALIER-KING-CHARLES-SPANIEL-136.html', 'http://www.fci.be/Nomenclature/Illustrations/136g09.jpg', 'http://www.fci.be/Nomenclature/Standards/136g09-en.pdf\r', NULL),
	(137, 'Pyrenean Mountain Dog', 'Molossian type', 'France', 'http://www.fci.be/en/nomenclature/PYRENEAN-MOUNTAIN-DOG-137.html', 'http://www.fci.be/Nomenclature/Illustrations/137g02.jpg', 'http://www.fci.be/Nomenclature/Standards/137g02-en.pdf\r', NULL),
	(138, 'Pyrenean Sheepdog - Smooth Faced', 'Sheepdogs', 'France', 'http://www.fci.be/en/nomenclature/PYRENEAN-SHEEPDOG-SMOOTH-FACED-138.html', 'http://www.fci.be/Nomenclature/Illustrations/138g01.jpg', 'http://www.fci.be/Nomenclature/Standards/138g01-en.pdf\r', NULL),
	(139, 'Irish Terrier', 'Large and medium sized Terriers', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-TERRIER-139.html', '', 'http://www.fci.be/Nomenclature/Standards/139g03-en.pdf\r', NULL),
	(140, 'Boston Terrier', 'Small Molossian type Dogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/BOSTON-TERRIER-140.html', '', 'http://www.fci.be/Nomenclature/Standards/140g09-en.pdf\r', NULL),
	(141, 'Long-haired Pyrenean Sheepdog', 'Sheepdogs', 'France', 'http://www.fci.be/en/nomenclature/LONG-HAIRED-PYRENEAN-SHEEPDOG-141.html', 'http://www.fci.be/Nomenclature/Illustrations/141g01.jpg', 'http://www.fci.be/Nomenclature/Standards/141g01-en.pdf\r', NULL),
	(142, 'Slovakian Chuvach', 'Sheepdogs', 'Slovakia', 'http://www.fci.be/en/nomenclature/SLOVAKIAN-CHUVACH-142.html', '', 'http://www.fci.be/Nomenclature/Standards/142g01-en.pdf\r', NULL),
	(143, 'Dobermann', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/DOBERMANN-143.html', 'http://www.fci.be/Nomenclature/Illustrations/143g02.jpg', 'http://www.fci.be/Nomenclature/Standards/143g02-en.pdf\r', NULL),
	(144, 'Boxer', 'Molossian type', 'Germany', 'http://www.fci.be/en/nomenclature/BOXER-144.html', 'http://www.fci.be/Nomenclature/Illustrations/144g02.jpg', 'http://www.fci.be/Nomenclature/Standards/144g02-en.pdf\r', NULL),
	(145, 'Leonberger', 'Molossian type', 'Germany', 'http://www.fci.be/en/nomenclature/LEONBERGER-145.html', '', 'http://www.fci.be/Nomenclature/Standards/145g02-en.pdf\r', NULL),
	(146, 'Rhodesian Ridgeback', 'Related breeds', 'South Africa', 'http://www.fci.be/en/nomenclature/RHODESIAN-RIDGEBACK-146.html', '', 'http://www.fci.be/Nomenclature/Standards/146g06-en.pdf\r', NULL),
	(147, 'Rottweiler', 'Molossian type', 'Germany', 'http://www.fci.be/en/nomenclature/ROTTWEILER-147.html', '', 'http://www.fci.be/Nomenclature/Standards/147g02-en.pdf\r', NULL),
	(148, 'Dachshund', '', 'Germany', 'http://www.fci.be/en/nomenclature/DACHSHUND-148.html', 'http://www.fci.be/Nomenclature/Illustrations/148g04-1.jpg', 'http://www.fci.be/Nomenclature/Standards/148g04-en.pdf\r', NULL),
	(149, 'Bulldog', 'Molossian type', 'Great Britain', 'http://www.fci.be/en/nomenclature/BULLDOG-149.html', 'http://www.fci.be/Nomenclature/Illustrations/149g02.jpg', 'http://www.fci.be/Nomenclature/Standards/149g02-en.pdf\r', NULL),
	(150, 'Serbian Hound', 'Scent hounds', 'Serbia', 'http://www.fci.be/en/nomenclature/SERBIAN-HOUND-150.html', 'http://www.fci.be/Nomenclature/Illustrations/150g06.jpg', 'http://www.fci.be/Nomenclature/Standards/150g06-en.pdf\r', NULL),
	(151, 'Istrian Short-haired Hound', 'Scent hounds', 'Croatia', 'http://www.fci.be/en/nomenclature/ISTRIAN-SHORT-HAIRED-HOUND-151.html', 'http://www.fci.be/Nomenclature/Illustrations/151g06.jpg', 'http://www.fci.be/Nomenclature/Standards/151g06-en.pdf\r', NULL),
	(152, 'Istrian Wire-haired Hound', 'Scent hounds', 'Croatia', 'http://www.fci.be/en/nomenclature/ISTRIAN-WIRE-HAIRED-HOUND-152.html', 'http://www.fci.be/Nomenclature/Illustrations/152g06.jpg', 'http://www.fci.be/Nomenclature/Standards/152g06-en.pdf\r', NULL),
	(153, 'Dalmatian', 'Related breeds', 'Croatia', 'http://www.fci.be/en/nomenclature/DALMATIAN-153.html', 'http://www.fci.be/Nomenclature/Illustrations/153g06.jpg', 'http://www.fci.be/Nomenclature/Standards/153g06-en.pdf\r', NULL),
	(154, 'Posavatz Hound', 'Scent hounds', 'Croatia', 'http://www.fci.be/en/nomenclature/POSAVATZ-HOUND-154.html', 'http://www.fci.be/Nomenclature/Illustrations/154g06.jpg', 'http://www.fci.be/Nomenclature/Standards/154g06-en.pdf\r', NULL),
	(155, 'Bosnian Broken-haired Hound - Called Barak', 'Scent hounds', 'Bosnia And Herzegovina', 'http://www.fci.be/en/nomenclature/BOSNIAN-BROKEN-HAIRED-HOUND-CALLED-BARAK-155.html', 'http://www.fci.be/Nomenclature/Illustrations/155g06.jpg', 'http://www.fci.be/Nomenclature/Standards/155g06-en.pdf\r', NULL),
	(156, 'Collie Rough', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/COLLIE-ROUGH-156.html', 'http://www.fci.be/Nomenclature/Illustrations/156g01.jpg', 'http://www.fci.be/Nomenclature/Standards/156g01-en.pdf\r', NULL),
	(157, 'Bullmastiff', 'Molossian type', 'Great Britain', 'http://www.fci.be/en/nomenclature/BULLMASTIFF-157.html', '', 'http://www.fci.be/Nomenclature/Standards/157g02-en.pdf\r', NULL),
	(158, 'Greyhound', 'Short-haired Sighthounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/GREYHOUND-158.html', 'http://www.fci.be/Nomenclature/Illustrations/158g10.jpg', 'http://www.fci.be/Nomenclature/Standards/158g10-en.pdf\r', NULL),
	(159, 'English Foxhound', 'Scent hounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/ENGLISH-FOXHOUND-159.html', '', 'http://www.fci.be/Nomenclature/Standards/159g06-en.pdf\r', NULL),
	(160, 'Irish Wolfhound', 'Rough-haired Sighthounds', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-WOLFHOUND-160.html', '', 'http://www.fci.be/Nomenclature/Standards/160g10-en.pdf\r', NULL),
	(161, 'Beagle', 'Scent hounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/BEAGLE-161.html', 'http://www.fci.be/Nomenclature/Illustrations/161g06.jpg', 'http://www.fci.be/Nomenclature/Standards/161g06-en.pdf\r', NULL),
	(162, 'Whippet', 'Short-haired Sighthounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/WHIPPET-162.html', 'http://www.fci.be/Nomenclature/Illustrations/162g10.jpg', 'http://www.fci.be/Nomenclature/Standards/162g10-en.pdf\r', NULL),
	(163, 'Basset Hound', 'Scent hounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/BASSET-HOUND-163.html', 'http://www.fci.be/Nomenclature/Illustrations/163g06.jpg', 'http://www.fci.be/Nomenclature/Standards/163g06-en.pdf\r', NULL),
	(164, 'Deerhound', 'Rough-haired Sighthounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/DEERHOUND-164.html', 'http://www.fci.be/Nomenclature/Illustrations/164g10.jpg', 'http://www.fci.be/Nomenclature/Standards/164g10-en.pdf\r', NULL),
	(165, 'Italian Spinone', 'Continental Pointing Dogs', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-SPINONE-165.html', '', 'http://www.fci.be/Nomenclature/Standards/165g07-en.pdf\r', NULL),
	(166, 'German Shepherd Dog', 'Sheepdogs', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-SHEPHERD-DOG-166.html', 'http://www.fci.be/Nomenclature/Illustrations/166g01-1.jpg', 'http://www.fci.be/Nomenclature/Standards/166g01-en.pdf\r', NULL),
	(167, 'American Cocker Spaniel', 'Flushing Dogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/AMERICAN-COCKER-SPANIEL-167.html', '', 'http://www.fci.be/Nomenclature/Standards/167g08-en.pdf\r', NULL),
	(168, 'Dandie Dinmont Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/DANDIE-DINMONT-TERRIER-168.html', 'http://www.fci.be/Nomenclature/Illustrations/168g03.jpg', 'http://www.fci.be/Nomenclature/Standards/168g03-en.pdf\r', NULL),
	(169, 'Fox Terrier (wire)', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/FOX-TERRIER-WIRE-169.html', 'http://www.fci.be/Nomenclature/Illustrations/169g03.jpg', 'http://www.fci.be/Nomenclature/Standards/169g03-en.pdf\r', NULL),
	(170, 'Castro Laboreiro Dog', 'Molossian type', 'Portugal', 'http://www.fci.be/en/nomenclature/CASTRO-LABOREIRO-DOG-170.html', 'http://www.fci.be/Nomenclature/Illustrations/170g02.jpg', 'http://www.fci.be/Nomenclature/Standards/170g02-en.pdf\r', NULL),
	(171, 'Bouvier Des Ardennes', 'Cattledogs (except Swiss Cattledogs)', 'Belgium', 'http://www.fci.be/en/nomenclature/BOUVIER-DES-ARDENNES-171.html', '', 'http://www.fci.be/Nomenclature/Standards/171g01-en.pdf\r', NULL),
	(172, 'Poodle', 'Poodle', 'France', 'http://www.fci.be/en/nomenclature/POODLE-172.html', 'http://www.fci.be/Nomenclature/Illustrations/172g09.jpg', 'http://www.fci.be/Nomenclature/Standards/172g09-en.pdf\r', NULL),
	(173, 'Estrela Mountain Dog', 'Molossian type', 'Portugal', 'http://www.fci.be/en/nomenclature/ESTRELA-MOUNTAIN-DOG-173.html', 'http://www.fci.be/Nomenclature/Illustrations/173g02.jpg', 'http://www.fci.be/Nomenclature/Standards/173g02-en.pdf\r', NULL),
	(175, 'French Spaniel', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-SPANIEL-175.html', 'http://www.fci.be/Nomenclature/Illustrations/175g07.jpg', 'http://www.fci.be/Nomenclature/Standards/175g07-en.pdf\r', NULL),
	(176, 'Picardy Sheepdog', 'Sheepdogs', 'France', 'http://www.fci.be/en/nomenclature/PICARDY-SHEEPDOG-176.html', '', 'http://www.fci.be/Nomenclature/Standards/176g01-en.pdf\r', NULL),
	(177, 'Ariege Pointing Dog', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/ARIEGE-POINTING-DOG-177.html', '', 'http://www.fci.be/Nomenclature/Standards/177g07-en.pdf\r', NULL),
	(179, 'Bourbonnais Pointing Dog', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/BOURBONNAIS-POINTING-DOG-179.html', 'http://www.fci.be/Nomenclature/Illustrations/179g07.jpg', 'http://www.fci.be/Nomenclature/Standards/179g07-en.pdf\r', NULL),
	(180, 'Auvergne Pointer', 'Continental Pointing Dogs', 'France', 'http://www.fci.be/en/nomenclature/AUVERGNE-POINTER-180.html', 'http://www.fci.be/Nomenclature/Illustrations/180g07.jpg', 'http://www.fci.be/Nomenclature/Standards/180g07-en.pdf\r', NULL),
	(181, 'Giant Schnauzer', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/GIANT-SCHNAUZER-181.html', 'http://www.fci.be/Nomenclature/Illustrations/181g02.jpg', 'http://www.fci.be/Nomenclature/Standards/181g02-en.pdf\r', NULL),
	(182, 'Schnauzer', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/SCHNAUZER-182.html', 'http://www.fci.be/Nomenclature/Illustrations/182g02.jpg', 'http://www.fci.be/Nomenclature/Standards/182g02-en.pdf\r', NULL),
	(183, 'Miniature Schnauzer', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/MINIATURE-SCHNAUZER-183.html', 'http://www.fci.be/Nomenclature/Illustrations/183g02.jpg', 'http://www.fci.be/Nomenclature/Standards/183g02-en.pdf\r', NULL),
	(184, 'German Pinscher', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-PINSCHER-184.html', 'http://www.fci.be/Nomenclature/Illustrations/184g02.jpg', 'http://www.fci.be/Nomenclature/Standards/184g02-en.pdf\r', NULL),
	(185, 'Miniature Pinscher', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/MINIATURE-PINSCHER-185.html', 'http://www.fci.be/Nomenclature/Illustrations/185g02.jpg', 'http://www.fci.be/Nomenclature/Standards/185g02-en.pdf\r', NULL),
	(186, 'Affenpinscher', 'Pinscher and Schnauzer type', 'Germany', 'http://www.fci.be/en/nomenclature/AFFENPINSCHER-186.html', 'http://www.fci.be/Nomenclature/Illustrations/186g02.jpg', 'http://www.fci.be/Nomenclature/Standards/186g02-en.pdf\r', NULL),
	(187, 'Portuguese Pointing Dog', 'Continental Pointing Dogs', 'Portugal', 'http://www.fci.be/en/nomenclature/PORTUGUESE-POINTING-DOG-187.html', 'http://www.fci.be/Nomenclature/Illustrations/187g07.jpg', 'http://www.fci.be/Nomenclature/Standards/187g07-en.pdf\r', NULL),
	(188, 'Sloughi', 'Short-haired Sighthounds', 'Morocco', 'http://www.fci.be/en/nomenclature/SLOUGHI-188.html', '', 'http://www.fci.be/Nomenclature/Standards/188g10-en.pdf\r', NULL),
	(189, 'Finnish Lapponian Dog', 'Nordic Watchdogs and Herders', 'Finland', 'http://www.fci.be/en/nomenclature/FINNISH-LAPPONIAN-DOG-189.html', 'http://www.fci.be/Nomenclature/Illustrations/189g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/189g05-en.pdf\r', NULL),
	(190, 'Hovawart', 'Molossian type', 'Germany', 'http://www.fci.be/en/nomenclature/HOVAWART-190.html', '', 'http://www.fci.be/Nomenclature/Standards/190g02-en.pdf\r', NULL),
	(191, 'Bouvier Des Flandres', 'Cattledogs (except Swiss Cattledogs)', 'Belgium, France', 'http://www.fci.be/en/nomenclature/BOUVIER-DES-FLANDRES-191.html', '', 'http://www.fci.be/Nomenclature/Standards/191g01-en.pdf\r', NULL),
	(192, 'Kromfohrländer', 'Kromfohrländer', 'Germany', 'http://www.fci.be/en/nomenclature/KROMFOHRLANDER-192.html', 'http://www.fci.be/Nomenclature/Illustrations/192g09.jpg', 'http://www.fci.be/Nomenclature/Standards/192g09-en.pdf\r', NULL),
	(193, 'Borzoi - Russian Hunting Sighthound', 'Long-haired or fringed Sighthounds', 'Russia', 'http://www.fci.be/en/nomenclature/BORZOI-RUSSIAN-HUNTING-SIGHTHOUND-193.html', 'http://www.fci.be/Nomenclature/Illustrations/193g10.jpg', 'http://www.fci.be/Nomenclature/Standards/193g10-en.pdf\r', NULL),
	(194, 'Bergamasco Shepherd Dog', 'Sheepdogs', 'Italy', 'http://www.fci.be/en/nomenclature/BERGAMASCO-SHEPHERD-DOG-194.html', 'http://www.fci.be/Nomenclature/Illustrations/194g01-1.jpg', 'http://www.fci.be/Nomenclature/Standards/194g01-en.pdf\r', NULL),
	(195, 'Italian Volpino', 'European Spitz', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-VOLPINO-195.html', '', 'http://www.fci.be/Nomenclature/Standards/195g05-en.pdf\r', NULL),
	(196, 'Bolognese', 'Bichons and related breeds', 'Italy', 'http://www.fci.be/en/nomenclature/BOLOGNESE-196.html', '', 'http://www.fci.be/Nomenclature/Standards/196g09-en.pdf\r', NULL),
	(197, 'Neapolitan Mastiff', 'Molossian type', 'Italy', 'http://www.fci.be/en/nomenclature/NEAPOLITAN-MASTIFF-197.html', '', 'http://www.fci.be/Nomenclature/Standards/197g02-en.pdf\r', NULL),
	(198, 'Italian Rough-haired Segugio', 'Scent hounds', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-ROUGH-HAIRED-SEGUGIO-198.html', '', 'http://www.fci.be/Nomenclature/Standards/198g06-en.pdf\r', NULL),
	(199, 'Cirneco Dell\'etna', 'Primitive type - Hunting Dogs', 'Italy', 'http://www.fci.be/en/nomenclature/CIRNECO-DELL-ETNA-199.html', '', 'http://www.fci.be/Nomenclature/Standards/199g05-en.pdf\r', NULL),
	(200, 'Italian Sighthound', 'Short-haired Sighthounds', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-SIGHTHOUND-200.html', '', 'http://www.fci.be/Nomenclature/Standards/200g10-en.pdf\r', NULL),
	(201, 'Maremma And The Abruzzes Sheepdog', 'Sheepdogs', 'Italy', 'http://www.fci.be/en/nomenclature/MAREMMA-AND-THE-ABRUZZES-SHEEPDOG-201.html', '', 'http://www.fci.be/Nomenclature/Standards/201g01-en.pdf\r', NULL),
	(202, 'Italian Pointing Dog', 'Continental Pointing Dogs', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-POINTING-DOG-202.html', 'http://www.fci.be/Nomenclature/Illustrations/202g07.jpg', 'http://www.fci.be/Nomenclature/Standards/202g07-en.pdf\r', NULL),
	(203, 'Norwegian Hound', 'Scent hounds', 'Norway', 'http://www.fci.be/en/nomenclature/NORWEGIAN-HOUND-203.html', 'http://www.fci.be/Nomenclature/Illustrations/203g06.jpg', 'http://www.fci.be/Nomenclature/Standards/203g06-en.pdf\r', NULL),
	(204, 'Spanish Hound', 'Scent hounds', 'Spain', 'http://www.fci.be/en/nomenclature/SPANISH-HOUND-204.html', '', 'http://www.fci.be/Nomenclature/Standards/204g06-en.pdf\r', NULL),
	(205, 'Chow Chow', 'Asian Spitz and related breeds', 'China', 'http://www.fci.be/en/nomenclature/CHOW-CHOW-205.html', 'http://www.fci.be/Nomenclature/Illustrations/205g05.jpg', 'http://www.fci.be/Nomenclature/Standards/205g05-en.pdf\r', NULL),
	(206, 'Japanese Chin', 'Japan Chin and Pekingese', 'Japan', 'http://www.fci.be/en/nomenclature/JAPANESE-CHIN-206.html', 'http://www.fci.be/Nomenclature/Illustrations/206g09.jpg', 'http://www.fci.be/Nomenclature/Standards/206g09-en.pdf\r', NULL),
	(207, 'Pekingese', 'Japan Chin and Pekingese', 'China', 'http://www.fci.be/en/nomenclature/PEKINGESE-207.html', 'http://www.fci.be/Nomenclature/Illustrations/207g09.jpg', 'http://www.fci.be/Nomenclature/Standards/207g09-en.pdf\r', NULL),
	(208, 'Shih Tzu', 'Tibetan breeds', 'Tibet (china)', 'http://www.fci.be/en/nomenclature/SHIH-TZU-208.html', 'http://www.fci.be/Nomenclature/Illustrations/208g09.jpg', 'http://www.fci.be/Nomenclature/Standards/208g09-en.pdf\r', NULL),
	(209, 'Tibetan Terrier', 'Tibetan breeds', 'Tibet (china)', 'http://www.fci.be/en/nomenclature/TIBETAN-TERRIER-209.html', 'http://www.fci.be/Nomenclature/Illustrations/209g09.jpg', 'http://www.fci.be/Nomenclature/Standards/209g09-en.pdf\r', NULL),
	(211, 'Canadian Eskimo Dog', 'Nordic Sledge Dogs', 'Canada', 'http://www.fci.be/en/nomenclature/CANADIAN-ESKIMO-DOG-211.html', 'http://www.fci.be/Nomenclature/Illustrations/211g05.jpg', 'http://www.fci.be/Nomenclature/Standards/211g05-en.pdf\r', NULL),
	(212, 'Samoyed', 'Nordic Sledge Dogs', 'Northern Russia, Siberia', 'http://www.fci.be/en/nomenclature/SAMOYED-212.html', 'http://www.fci.be/Nomenclature/Illustrations/212g05.jpg', 'http://www.fci.be/Nomenclature/Standards/212g05-en.pdf\r', NULL),
	(213, 'Hanoverian Scent Hound', 'Leash (scent) Hounds', 'Germany', 'http://www.fci.be/en/nomenclature/HANOVERIAN-SCENT-HOUND-213.html', 'http://www.fci.be/Nomenclature/Illustrations/213g06-1.jpg', 'http://www.fci.be/Nomenclature/Standards/213g06-en.pdf\r', NULL),
	(214, 'Hellenic Hound', 'Scent hounds', 'Greece', 'http://www.fci.be/en/nomenclature/HELLENIC-HOUND-214.html', '', 'http://www.fci.be/Nomenclature/Standards/214g06-en.pdf\r', NULL),
	(215, 'Bichon Frise', 'Bichons and related breeds', 'Belgium, France', 'http://www.fci.be/en/nomenclature/BICHON-FRISE-215.html', 'http://www.fci.be/Nomenclature/Illustrations/215g09-1.jpg', 'http://www.fci.be/Nomenclature/Standards/215g09-en.pdf\r', NULL),
	(216, 'Pudelpointer', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/PUDELPOINTER-216.html', '', 'http://www.fci.be/Nomenclature/Standards/216g07-en.pdf\r', NULL),
	(217, 'Bavarian Mountain Scent Hound', 'Leash (scent) Hounds', 'Germany', 'http://www.fci.be/en/nomenclature/BAVARIAN-MOUNTAIN-SCENT-HOUND-217.html', 'http://www.fci.be/Nomenclature/Illustrations/217g06-1.jpg', 'http://www.fci.be/Nomenclature/Standards/217g06-en.pdf\r', NULL),
	(218, 'Chihuahua', 'Chihuahueno', 'Mexico', 'http://www.fci.be/en/nomenclature/CHIHUAHUA-218.html', 'http://www.fci.be/Nomenclature/Illustrations/218g09-1.jpg', 'http://www.fci.be/Nomenclature/Standards/218g09-en.pdf\r', NULL),
	(219, 'French Tricolour Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-TRICOLOUR-HOUND-219.html', '', 'http://www.fci.be/Nomenclature/Standards/219g06-en.pdf\r', NULL),
	(220, 'French White & Black Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-WHITE-BLACK-HOUND-220.html', 'http://www.fci.be/Nomenclature/Illustrations/220g06.jpg', 'http://www.fci.be/Nomenclature/Standards/220g06-en.pdf\r', NULL),
	(221, 'Frisian Water Dog', 'Water Dogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/FRISIAN-WATER-DOG-221.html', '', 'http://www.fci.be/Nomenclature/Standards/221g08-en.pdf\r', NULL),
	(222, 'Stabijhoun', 'Continental Pointing Dogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/STABIJHOUN-222.html', 'http://www.fci.be/Nomenclature/Illustrations/222g07-1.jpg', 'http://www.fci.be/Nomenclature/Standards/222g07-en.pdf\r', NULL),
	(223, 'Dutch Shepherd Dog', 'Sheepdogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/DUTCH-SHEPHERD-DOG-223.html', 'http://www.fci.be/Nomenclature/Illustrations/223g01-1.jpg', 'http://www.fci.be/Nomenclature/Standards/223g01-en.pdf\r', NULL),
	(224, 'Drentsche Partridge Dog', 'Continental Pointing Dogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/DRENTSCHE-PARTRIDGE-DOG-224.html', 'http://www.fci.be/Nomenclature/Illustrations/224g07-1.jpg', 'http://www.fci.be/Nomenclature/Standards/224g07-en.pdf\r', NULL),
	(225, 'Fila Brasileiro', 'Molossian type', 'Brazil', 'http://www.fci.be/en/nomenclature/FILA-BRASILEIRO-225.html', 'http://www.fci.be/Nomenclature/Illustrations/225g02.jpg', 'http://www.fci.be/Nomenclature/Standards/225g02-en.pdf\r', NULL),
	(226, 'Landseer (european Continental Type)', 'Molossian type', 'Germany, Switzerland', 'http://www.fci.be/en/nomenclature/LANDSEER-EUROPEAN-CONTINENTAL-TYPE-226.html', '', 'http://www.fci.be/Nomenclature/Standards/226g02-en.pdf\r', NULL),
	(227, 'Lhasa Apso', 'Tibetan breeds', 'Tibet (china)', 'http://www.fci.be/en/nomenclature/LHASA-APSO-227.html', 'http://www.fci.be/Nomenclature/Illustrations/227g09.jpg', 'http://www.fci.be/Nomenclature/Standards/227g09-en.pdf\r', NULL),
	(228, 'Afghan Hound', 'Long-haired or fringed Sighthounds', 'Afghanistan', 'http://www.fci.be/en/nomenclature/AFGHAN-HOUND-228.html', 'http://www.fci.be/Nomenclature/Illustrations/228g10.jpg', 'http://www.fci.be/Nomenclature/Standards/228g10-en.pdf\r', NULL),
	(229, 'Serbian Tricolour Hound', 'Scent hounds', 'Serbia', 'http://www.fci.be/en/nomenclature/SERBIAN-TRICOLOUR-HOUND-229.html', 'http://www.fci.be/Nomenclature/Illustrations/229g06.jpg', 'http://www.fci.be/Nomenclature/Standards/229g06-en.pdf\r', NULL),
	(230, 'Tibetan Mastiff', 'Molossian type', 'Tibet (china)', 'http://www.fci.be/en/nomenclature/TIBETAN-MASTIFF-230.html', '', 'http://www.fci.be/Nomenclature/Standards/230g02-en.pdf\r', NULL),
	(231, 'Tibetan Spaniel', 'Tibetan breeds', 'Tibet (china)', 'http://www.fci.be/en/nomenclature/TIBETAN-SPANIEL-231.html', 'http://www.fci.be/Nomenclature/Illustrations/231g09.jpg', 'http://www.fci.be/Nomenclature/Standards/231g09-en.pdf\r', NULL),
	(232, 'Deutsch Stichelhaar', 'Continental Pointing Dogs', 'Germany', 'http://www.fci.be/en/nomenclature/DEUTSCH-STICHELHAAR-232.html', 'http://www.fci.be/Nomenclature/Illustrations/232g07.jpg', 'http://www.fci.be/Nomenclature/Standards/232g07-en.pdf\r', NULL),
	(233, 'Little Lion Dog', 'Bichons and related breeds', 'France', 'http://www.fci.be/en/nomenclature/LITTLE-LION-DOG-233.html', 'http://www.fci.be/Nomenclature/Illustrations/233g09.jpg', 'http://www.fci.be/Nomenclature/Standards/233g09-en.pdf\r', NULL),
	(234, 'Xoloitzcuintle', 'Primitive type', 'Mexico', 'http://www.fci.be/en/nomenclature/XOLOITZCUINTLE-234.html', 'http://www.fci.be/Nomenclature/Illustrations/234g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/234g05-en.pdf\r', NULL),
	(235, 'Great Dane', 'Molossian type', 'Germany', 'http://www.fci.be/en/nomenclature/GREAT-DANE-235.html', 'http://www.fci.be/Nomenclature/Illustrations/235g02.jpg', 'http://www.fci.be/Nomenclature/Standards/235g02-en.pdf\r', NULL),
	(236, 'Australian Silky Terrier', 'Toy Terriers', 'Australia', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-SILKY-TERRIER-236.html', 'http://www.fci.be/Nomenclature/Illustrations/236g03.jpg', 'http://www.fci.be/Nomenclature/Standards/236g03-en.pdf\r', NULL),
	(237, 'Norwegian Buhund', 'Nordic Watchdogs and Herders', 'Norway', 'http://www.fci.be/en/nomenclature/NORWEGIAN-BUHUND-237.html', 'http://www.fci.be/Nomenclature/Illustrations/237g05.jpg', 'http://www.fci.be/Nomenclature/Standards/237g05-en.pdf\r', NULL),
	(238, 'Mudi', 'Sheepdogs', 'Hungary', 'http://www.fci.be/en/nomenclature/MUDI-238.html', 'http://www.fci.be/Nomenclature/Illustrations/238g01.jpg', 'http://www.fci.be/Nomenclature/Standards/238g01-en.pdf\r', NULL),
	(239, 'Hungarian Wire-haired Pointer', 'Continental Pointing Dogs', 'Hungary', 'http://www.fci.be/en/nomenclature/HUNGARIAN-WIRE-HAIRED-POINTER-239.html', 'http://www.fci.be/Nomenclature/Illustrations/239g07.jpg', 'http://www.fci.be/Nomenclature/Standards/239g07-en.pdf\r', NULL),
	(240, 'Hungarian Greyhound', 'Short-haired Sighthounds', 'Hungary', 'http://www.fci.be/en/nomenclature/HUNGARIAN-GREYHOUND-240.html', 'http://www.fci.be/Nomenclature/Illustrations/240g10.jpg', 'http://www.fci.be/Nomenclature/Standards/240g10-en.pdf\r', NULL),
	(241, 'Hungarian Hound - Transylvanian Scent Hound', 'Scent hounds', 'Hungary', 'http://www.fci.be/en/nomenclature/HUNGARIAN-HOUND-TRANSYLVANIAN-SCENT-HOUND-241.html', 'http://www.fci.be/Nomenclature/Illustrations/241g06.jpg', 'http://www.fci.be/Nomenclature/Standards/241g06-en.pdf\r', NULL),
	(242, 'Norwegian Elkhound Grey', 'Nordic Hunting Dogs', 'Norway', 'http://www.fci.be/en/nomenclature/NORWEGIAN-ELKHOUND-GREY-242.html', 'http://www.fci.be/Nomenclature/Illustrations/242g05.jpg', 'http://www.fci.be/Nomenclature/Standards/242g05-en.pdf\r', NULL),
	(243, 'Alaskan Malamute', 'Nordic Sledge Dogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/ALASKAN-MALAMUTE-243.html', '', 'http://www.fci.be/Nomenclature/Standards/243g05-en.pdf\r', NULL),
	(244, 'Slovakian Hound', 'Scent hounds', 'Slovakia', 'http://www.fci.be/en/nomenclature/SLOVAKIAN-HOUND-244.html', '', 'http://www.fci.be/Nomenclature/Standards/244g06-en.pdf\r', NULL),
	(245, 'Bohemian Wire-haired Pointing Griffon', 'Continental Pointing Dogs', 'Czech Republic', 'http://www.fci.be/en/nomenclature/BOHEMIAN-WIRE-HAIRED-POINTING-GRIFFON-245.html', '', 'http://www.fci.be/Nomenclature/Standards/245g07-en.pdf\r', NULL),
	(246, 'Cesky Terrier', 'Small sized Terriers', 'Czech Republic', 'http://www.fci.be/en/nomenclature/CESKY-TERRIER-246.html', 'http://www.fci.be/Nomenclature/Illustrations/246g03.jpg', 'http://www.fci.be/Nomenclature/Standards/246g03-en.pdf\r', NULL),
	(247, 'Atlas Mountain Dog (aidi)', 'Molossian type', 'Morocco', 'http://www.fci.be/en/nomenclature/ATLAS-MOUNTAIN-DOG-AIDI-247.html', '', 'http://www.fci.be/Nomenclature/Standards/247g02-en.pdf\r', NULL),
	(248, 'Pharaoh Hound', 'Primitive type', 'Malta', 'http://www.fci.be/en/nomenclature/PHARAOH-HOUND-248.html', '', 'http://www.fci.be/Nomenclature/Standards/248g05-en.pdf\r', NULL),
	(249, 'Majorca Mastiff', 'Molossian type', 'Spain', 'http://www.fci.be/en/nomenclature/MAJORCA-MASTIFF-249.html', '', 'http://www.fci.be/Nomenclature/Standards/249g02-en.pdf\r', NULL),
	(250, 'Havanese', 'Bichons and related breeds', 'Cuba', 'http://www.fci.be/en/nomenclature/HAVANESE-250.html', '', 'http://www.fci.be/Nomenclature/Standards/250g09-en.pdf\r', NULL),
	(251, 'Polish Lowland Sheepdog', 'Sheepdogs', 'Poland', 'http://www.fci.be/en/nomenclature/POLISH-LOWLAND-SHEEPDOG-251.html', 'http://www.fci.be/Nomenclature/Illustrations/251g01.jpg', 'http://www.fci.be/Nomenclature/Standards/251g01-en.pdf\r', NULL),
	(252, 'Tatra Shepherd Dog', 'Sheepdogs', 'Poland', 'http://www.fci.be/en/nomenclature/TATRA-SHEPHERD-DOG-252.html', 'http://www.fci.be/Nomenclature/Illustrations/252g01.jpg', 'http://www.fci.be/Nomenclature/Standards/252g01-en.pdf\r', NULL),
	(253, 'Pug', 'Small Molossian type Dogs', 'China', 'http://www.fci.be/en/nomenclature/PUG-253.html', 'http://www.fci.be/Nomenclature/Illustrations/253g09.jpg', 'http://www.fci.be/Nomenclature/Standards/253g09-en.pdf\r', NULL),
	(254, 'Alpine Dachsbracke', 'Leash (scent) Hounds', 'Austria', 'http://www.fci.be/en/nomenclature/ALPINE-DACHSBRACKE-254.html', '', 'http://www.fci.be/Nomenclature/Standards/254g06-en.pdf\r', NULL),
	(255, 'Akita', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/AKITA-255.html', 'http://www.fci.be/Nomenclature/Illustrations/255g05.jpg', 'http://www.fci.be/Nomenclature/Standards/255g05-en.pdf\r', NULL),
	(257, 'Shiba', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/SHIBA-257.html', 'http://www.fci.be/Nomenclature/Illustrations/257g05.jpg', 'http://www.fci.be/Nomenclature/Standards/257g05-en.pdf\r', NULL),
	(259, 'Japanese Terrier', 'Small sized Terriers', 'Japan', 'http://www.fci.be/en/nomenclature/JAPANESE-TERRIER-259.html', 'http://www.fci.be/Nomenclature/Illustrations/259g03.jpg', 'http://www.fci.be/Nomenclature/Standards/259g03-en.pdf\r', NULL),
	(260, 'Tosa', 'Molossian type', 'Japan', 'http://www.fci.be/en/nomenclature/TOSA-260.html', 'http://www.fci.be/Nomenclature/Illustrations/260g02.jpg', 'http://www.fci.be/Nomenclature/Standards/260g02-en.pdf\r', NULL),
	(261, 'Hokkaido', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/HOKKAIDO-261.html', 'http://www.fci.be/Nomenclature/Illustrations/261g05.jpg', 'http://www.fci.be/Nomenclature/Standards/261g05-en.pdf\r', NULL),
	(262, 'Japanese Spitz', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/JAPANESE-SPITZ-262.html', 'http://www.fci.be/Nomenclature/Illustrations/262g05.jpg', 'http://www.fci.be/Nomenclature/Standards/262g05-en.pdf\r', NULL),
	(263, 'Chesapeake Bay Retriever', 'Retrievers', 'United States Of America', 'http://www.fci.be/en/nomenclature/CHESAPEAKE-BAY-RETRIEVER-263.html', '', 'http://www.fci.be/Nomenclature/Standards/263g08-en.pdf\r', NULL),
	(264, 'Mastiff', 'Molossian type', 'Great Britain', 'http://www.fci.be/en/nomenclature/MASTIFF-264.html', '', 'http://www.fci.be/Nomenclature/Standards/264g02-en.pdf\r', NULL),
	(265, 'Norwegian Lundehund', 'Nordic Hunting Dogs', 'Norway', 'http://www.fci.be/en/nomenclature/NORWEGIAN-LUNDEHUND-265.html', 'http://www.fci.be/Nomenclature/Illustrations/265g05.jpg', 'http://www.fci.be/Nomenclature/Standards/265g05-en.pdf\r', NULL),
	(266, 'Hygen Hound', 'Scent hounds', 'Norway', 'http://www.fci.be/en/nomenclature/HYGEN-HOUND-266.html', 'http://www.fci.be/Nomenclature/Illustrations/266g06.jpg', 'http://www.fci.be/Nomenclature/Standards/266g06-en.pdf\r', NULL),
	(267, 'Halden Hound', 'Scent hounds', 'Norway', 'http://www.fci.be/en/nomenclature/HALDEN-HOUND-267.html', 'http://www.fci.be/Nomenclature/Illustrations/267g06.jpg', 'http://www.fci.be/Nomenclature/Standards/267g06-en.pdf\r', NULL),
	(268, 'Norwegian Elkhound Black', 'Nordic Hunting Dogs', 'Norway', 'http://www.fci.be/en/nomenclature/NORWEGIAN-ELKHOUND-BLACK-268.html', 'http://www.fci.be/Nomenclature/Illustrations/268g05.jpg', 'http://www.fci.be/Nomenclature/Standards/268g05-en.pdf\r', NULL),
	(269, 'Saluki', 'Long-haired or fringed Sighthounds', 'Middle East', 'http://www.fci.be/en/nomenclature/SALUKI-269.html', '', 'http://www.fci.be/Nomenclature/Standards/269g10-en.pdf\r', NULL),
	(270, 'Siberian Husky', 'Nordic Sledge Dogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/SIBERIAN-HUSKY-270.html', '', 'http://www.fci.be/Nomenclature/Standards/270g05-en.pdf\r', NULL),
	(271, 'Bearded Collie', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/BEARDED-COLLIE-271.html', 'http://www.fci.be/Nomenclature/Illustrations/271g01.jpg', 'http://www.fci.be/Nomenclature/Standards/271g01-en.pdf\r', NULL),
	(272, 'Norfolk Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/NORFOLK-TERRIER-272.html', 'http://www.fci.be/Nomenclature/Illustrations/272g03.jpg', 'http://www.fci.be/Nomenclature/Standards/272g03-en.pdf\r', NULL),
	(273, 'Canaan Dog', 'Primitive type', 'Israel', 'http://www.fci.be/en/nomenclature/CANAAN-DOG-273.html', '', 'http://www.fci.be/Nomenclature/Standards/273g05-en.pdf\r', NULL),
	(274, 'Greenland Dog', 'Nordic Sledge Dogs', 'Greenland', 'http://www.fci.be/en/nomenclature/GREENLAND-DOG-274.html', 'http://www.fci.be/Nomenclature/Illustrations/274g05.jpg', 'http://www.fci.be/Nomenclature/Standards/274g05-en.pdf\r', NULL),
	(276, 'Norrbottenspitz', 'Nordic Hunting Dogs', 'Sweden', 'http://www.fci.be/en/nomenclature/NORRBOTTENSPITZ-276.html', 'http://www.fci.be/Nomenclature/Illustrations/276g05.jpg', 'http://www.fci.be/Nomenclature/Standards/276g05-en.pdf\r', NULL),
	(277, 'Croatian Shepherd Dog', 'Sheepdogs', 'Croatia', 'http://www.fci.be/en/nomenclature/CROATIAN-SHEPHERD-DOG-277.html', 'http://www.fci.be/Nomenclature/Illustrations/277g01.jpg', 'http://www.fci.be/Nomenclature/Standards/277g01-en.pdf\r', NULL),
	(278, 'Karst Shepherd Dog', 'Molossian type', 'Slovenia', 'http://www.fci.be/en/nomenclature/KARST-SHEPHERD-DOG-278.html', 'http://www.fci.be/Nomenclature/Illustrations/278g02.jpg', 'http://www.fci.be/Nomenclature/Standards/278g02-en.pdf\r', NULL),
	(279, 'Montenegrin Mountain Hound', 'Scent hounds', 'Montenegro', 'http://www.fci.be/en/nomenclature/MONTENEGRIN-MOUNTAIN-HOUND-279.html', 'http://www.fci.be/Nomenclature/Illustrations/279g06.jpg', 'http://www.fci.be/Nomenclature/Standards/279g06-en.pdf\r', NULL),
	(281, 'Old Danish Pointing Dog', 'Continental Pointing Dogs', 'Denmark', 'http://www.fci.be/en/nomenclature/OLD-DANISH-POINTING-DOG-281.html', '', 'http://www.fci.be/Nomenclature/Standards/281g07-en.pdf\r', NULL),
	(282, 'Grand Griffon Vendeen', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GRAND-GRIFFON-VENDEEN-282.html', 'http://www.fci.be/Nomenclature/Illustrations/282g06.jpg', 'http://www.fci.be/Nomenclature/Standards/282g06-en.pdf\r', NULL),
	(283, 'Coton De Tulear', 'Bichons and related breeds', 'Madagascar', 'http://www.fci.be/en/nomenclature/COTON-DE-TULEAR-283.html', '', 'http://www.fci.be/Nomenclature/Standards/283g09-en.pdf\r', NULL),
	(284, 'Lapponian Herder', 'Nordic Watchdogs and Herders', 'Finland', 'http://www.fci.be/en/nomenclature/LAPPONIAN-HERDER-284.html', '', 'http://www.fci.be/Nomenclature/Standards/284g05-en.pdf\r', NULL),
	(285, 'Spanish Greyhound', 'Short-haired Sighthounds', 'Spain', 'http://www.fci.be/en/nomenclature/SPANISH-GREYHOUND-285.html', '', 'http://www.fci.be/Nomenclature/Standards/285g10-en.pdf\r', NULL),
	(286, 'American Staffordshire Terrier', 'Bull type Terriers', 'United States Of America', 'http://www.fci.be/en/nomenclature/AMERICAN-STAFFORDSHIRE-TERRIER-286.html', '', 'http://www.fci.be/Nomenclature/Standards/286g03-en.pdf\r', NULL),
	(287, 'Australian Cattle Dog', 'Cattledogs (except Swiss Cattledogs)', 'Australia', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-CATTLE-DOG-287.html', 'http://www.fci.be/Nomenclature/Illustrations/287g01.jpg', 'http://www.fci.be/Nomenclature/Standards/287g01-en.pdf\r', NULL),
	(288, 'Chinese Crested Dog', 'Hairless Dogs', 'China', 'http://www.fci.be/en/nomenclature/CHINESE-CRESTED-DOG-288.html', 'http://www.fci.be/Nomenclature/Illustrations/288g09.jpg', 'http://www.fci.be/Nomenclature/Standards/288g09-en.pdf\r', NULL),
	(289, 'Icelandic Sheepdog', 'Nordic Watchdogs and Herders', 'Iceland', 'http://www.fci.be/en/nomenclature/ICELANDIC-SHEEPDOG-289.html', 'http://www.fci.be/Nomenclature/Illustrations/289g05.jpg', 'http://www.fci.be/Nomenclature/Standards/289g05-en.pdf\r', NULL),
	(290, 'Beagle Harrier', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/BEAGLE-HARRIER-290.html', '', 'http://www.fci.be/Nomenclature/Standards/290g06-en.pdf\r', NULL),
	(291, 'Eurasian', 'Asian Spitz and related breeds', 'Germany', 'http://www.fci.be/en/nomenclature/EURASIAN-291.html', 'http://www.fci.be/Nomenclature/Illustrations/291g05.jpg', 'http://www.fci.be/Nomenclature/Standards/291g05-en.pdf\r', NULL),
	(292, 'Dogo Argentino', 'Molossian type', 'Argentina', 'http://www.fci.be/en/nomenclature/DOGO-ARGENTINO-292.html', 'http://www.fci.be/Nomenclature/Illustrations/292g02.jpg', 'http://www.fci.be/Nomenclature/Standards/292g02-en.pdf\r', NULL),
	(293, 'Australian Kelpie', 'Sheepdogs', 'Australia', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-KELPIE-293.html', 'http://www.fci.be/Nomenclature/Illustrations/293g01.jpg', 'http://www.fci.be/Nomenclature/Standards/293g01-en.pdf\r', NULL),
	(294, 'Otterhound', 'Scent hounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/OTTERHOUND-294.html', 'http://www.fci.be/Nomenclature/Illustrations/294g06.jpg', 'http://www.fci.be/Nomenclature/Standards/294g06-en.pdf\r', NULL),
	(295, 'Harrier', 'Scent hounds', 'Great Britain', 'http://www.fci.be/en/nomenclature/HARRIER-295.html', '', 'http://www.fci.be/Nomenclature/Standards/295g06-en.pdf\r', NULL),
	(296, 'Collie Smooth', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/COLLIE-SMOOTH-296.html', 'http://www.fci.be/Nomenclature/Illustrations/296g01.jpg', 'http://www.fci.be/Nomenclature/Standards/296g01-en.pdf\r', NULL),
	(297, 'Border Collie', 'Sheepdogs', 'Great Britain', 'http://www.fci.be/en/nomenclature/BORDER-COLLIE-297.html', 'http://www.fci.be/Nomenclature/Illustrations/297g01.jpg', 'http://www.fci.be/Nomenclature/Standards/297g01-en.pdf\r', NULL),
	(298, 'Romagna Water Dog', 'Water Dogs', 'Italy', 'http://www.fci.be/en/nomenclature/ROMAGNA-WATER-DOG-298.html', 'http://www.fci.be/Nomenclature/Illustrations/298g08-1.jpg', 'http://www.fci.be/Nomenclature/Standards/298g08-en.pdf\r', NULL),
	(299, 'German Hound', 'Scent hounds', 'Germany', 'http://www.fci.be/en/nomenclature/GERMAN-HOUND-299.html', '', 'http://www.fci.be/Nomenclature/Standards/299g06-en.pdf\r', NULL),
	(300, 'Black And Tan Coonhound', 'Scent hounds', 'United States Of America', 'http://www.fci.be/en/nomenclature/BLACK-AND-TAN-COONHOUND-300.html', 'http://www.fci.be/Nomenclature/Illustrations/300g06.jpg', 'http://www.fci.be/Nomenclature/Standards/300g06-en.pdf\r', NULL),
	(301, 'American Water Spaniel', 'Water Dogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/AMERICAN-WATER-SPANIEL-301.html', '', 'http://www.fci.be/Nomenclature/Standards/301g08-en.pdf\r', NULL),
	(302, 'Irish Glen Of Imaal Terrier', 'Large and medium sized Terriers', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-GLEN-OF-IMAAL-TERRIER-302.html', '', 'http://www.fci.be/Nomenclature/Standards/302g03-en.pdf\r', NULL),
	(303, 'American Foxhound', 'Scent hounds', 'United States Of America', 'http://www.fci.be/en/nomenclature/AMERICAN-FOXHOUND-303.html', '', 'http://www.fci.be/Nomenclature/Standards/303g06-en.pdf\r', NULL),
	(304, 'Russian-european Laika', 'Nordic Hunting Dogs', 'Russia', 'http://www.fci.be/en/nomenclature/RUSSIAN-EUROPEAN-LAIKA-304.html', '', 'http://www.fci.be/Nomenclature/Standards/304g05-en.pdf\r', NULL),
	(305, 'East Siberian Laika', 'Nordic Hunting Dogs', 'Russia', 'http://www.fci.be/en/nomenclature/EAST-SIBERIAN-LAIKA-305.html', 'http://www.fci.be/Nomenclature/Illustrations/305g05-1.jpg', 'http://www.fci.be/Nomenclature/Standards/305g05-en.pdf\r', NULL),
	(306, 'West Siberian Laika', 'Nordic Hunting Dogs', 'Russia', 'http://www.fci.be/en/nomenclature/WEST-SIBERIAN-LAIKA-306.html', '', 'http://www.fci.be/Nomenclature/Standards/306g05-en.pdf\r', NULL),
	(307, 'Azawakh', 'Short-haired Sighthounds', 'Mali', 'http://www.fci.be/en/nomenclature/AZAWAKH-307.html', 'http://www.fci.be/Nomenclature/Illustrations/307g10-1.jpg', 'http://www.fci.be/Nomenclature/Standards/307g10-en.pdf\r', NULL),
	(308, 'Dutch Smoushond', 'Pinscher and Schnauzer type', 'The Netherlands', 'http://www.fci.be/en/nomenclature/DUTCH-SMOUSHOND-308.html', '', 'http://www.fci.be/Nomenclature/Standards/308g02-en.pdf\r', NULL),
	(309, 'Shar Pei', 'Molossian type', 'China', 'http://www.fci.be/en/nomenclature/SHAR-PEI-309.html', '', 'http://www.fci.be/Nomenclature/Standards/309g02-en.pdf\r', NULL),
	(310, 'Peruvian Hairless Dog', 'Primitive type', 'Peru', 'http://www.fci.be/en/nomenclature/PERUVIAN-HAIRLESS-DOG-310.html', 'http://www.fci.be/Nomenclature/Illustrations/310g05.jpg', 'http://www.fci.be/Nomenclature/Standards/310g05-en.pdf\r', NULL),
	(311, 'Saarloos Wolfhond', 'Sheepdogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/SAARLOOS-WOLFHOND-311.html', 'http://www.fci.be/Nomenclature/Illustrations/311g01-1.jpg', 'http://www.fci.be/Nomenclature/Standards/311g01-en.pdf\r', NULL),
	(312, 'Nova Scotia Duck Tolling Retriever', 'Retrievers', 'Canada', 'http://www.fci.be/en/nomenclature/NOVA-SCOTIA-DUCK-TOLLING-RETRIEVER-312.html', '', 'http://www.fci.be/Nomenclature/Standards/312g08-en.pdf\r', NULL),
	(313, 'Dutch Schapendoes', 'Sheepdogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/DUTCH-SCHAPENDOES-313.html', 'http://www.fci.be/Nomenclature/Illustrations/313g01.jpg', 'http://www.fci.be/Nomenclature/Standards/313g01-en.pdf\r', NULL),
	(314, 'Nederlandse Kooikerhondje', 'Flushing Dogs', 'The Netherlands', 'http://www.fci.be/en/nomenclature/NEDERLANDSE-KOOIKERHONDJE-314.html', 'http://www.fci.be/Nomenclature/Illustrations/314g08-1.jpg', 'http://www.fci.be/Nomenclature/Standards/314g08-en.pdf\r', NULL),
	(315, 'Broholmer', 'Molossian type', 'Denmark', 'http://www.fci.be/en/nomenclature/BROHOLMER-315.html', 'http://www.fci.be/Nomenclature/Illustrations/315g02.jpg', 'http://www.fci.be/Nomenclature/Standards/315g02-en.pdf\r', NULL),
	(316, 'French White And Orange Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/FRENCH-WHITE-AND-ORANGE-HOUND-316.html', '', 'http://www.fci.be/Nomenclature/Standards/316g06-en.pdf\r', NULL),
	(317, 'Kai', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/KAI-317.html', 'http://www.fci.be/Nomenclature/Illustrations/317g05.jpg', 'http://www.fci.be/Nomenclature/Standards/317g05-en.pdf\r', NULL),
	(318, 'Kishu', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/KISHU-318.html', 'http://www.fci.be/Nomenclature/Illustrations/318g05.jpg', 'http://www.fci.be/Nomenclature/Standards/318g05-en.pdf\r', NULL),
	(319, 'Shikoku', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/SHIKOKU-319.html', 'http://www.fci.be/Nomenclature/Illustrations/319g05.jpg', 'http://www.fci.be/Nomenclature/Standards/319g05-en.pdf\r', NULL),
	(320, 'Wirehaired Slovakian Pointer', 'Continental Pointing Dogs', 'Slovakia', 'http://www.fci.be/en/nomenclature/WIREHAIRED-SLOVAKIAN-POINTER-320.html', '', 'http://www.fci.be/Nomenclature/Standards/320g07-en.pdf\r', NULL),
	(321, 'Majorca Shepherd Dog', 'Sheepdogs', 'Spain', 'http://www.fci.be/en/nomenclature/MAJORCA-SHEPHERD-DOG-321.html', '', '\r', NULL),
	(322, 'Great Anglo-french Tricolour Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GREAT-ANGLO-FRENCH-TRICOLOUR-HOUND-322.html', '', 'http://www.fci.be/Nomenclature/Standards/322g06-en.pdf\r', NULL),
	(323, 'Great Anglo-french White And Black Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GREAT-ANGLO-FRENCH-WHITE-AND-BLACK-HOUND-323.html', 'http://www.fci.be/Nomenclature/Illustrations/323g06.jpg', 'http://www.fci.be/Nomenclature/Standards/323g06-en.pdf\r', NULL),
	(324, 'Great Anglo-french White & Orange Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/GREAT-ANGLO-FRENCH-WHITE-ORANGE-HOUND-324.html', '', 'http://www.fci.be/Nomenclature/Standards/324g06-en.pdf\r', NULL),
	(325, 'Medium-sized Anglo-french Hound', 'Scent hounds', 'France', 'http://www.fci.be/en/nomenclature/MEDIUM-SIZED-ANGLO-FRENCH-HOUND-325.html', '', 'http://www.fci.be/Nomenclature/Standards/325g06-en.pdf\r', NULL),
	(326, 'South Russian Shepherd Dog', 'Sheepdogs', 'Russia', 'http://www.fci.be/en/nomenclature/SOUTH-RUSSIAN-SHEPHERD-DOG-326.html', '', 'http://www.fci.be/Nomenclature/Standards/326g01-en.pdf\r', NULL),
	(327, 'Russian Black Terrier', 'Pinscher and Schnauzer type', 'Russia', 'http://www.fci.be/en/nomenclature/RUSSIAN-BLACK-TERRIER-327.html', '', 'http://www.fci.be/Nomenclature/Standards/327g02-en.pdf\r', NULL),
	(328, 'Caucasian Shepherd Dog', 'Molossian type', 'Russia', 'http://www.fci.be/en/nomenclature/CAUCASIAN-SHEPHERD-DOG-328.html', 'http://www.fci.be/Nomenclature/Illustrations/328g02.jpg', 'http://www.fci.be/Nomenclature/Standards/328g02-en.pdf\r', NULL),
	(329, 'Canarian Warren Hound', 'Primitive type - Hunting Dogs', 'Spain', 'http://www.fci.be/en/nomenclature/CANARIAN-WARREN-HOUND-329.html', 'http://www.fci.be/Nomenclature/Illustrations/329g05.jpg', 'http://www.fci.be/Nomenclature/Standards/329g05-en.pdf\r', NULL),
	(330, 'Irish Red And White Setter', 'British and Irish Pointers and Setters', 'Ireland', 'http://www.fci.be/en/nomenclature/IRISH-RED-AND-WHITE-SETTER-330.html', '', 'http://www.fci.be/Nomenclature/Standards/330g07-en.pdf\r', NULL),
	(331, 'Kangal Shepherd Dog', 'Molossian type', 'Turkey', 'http://www.fci.be/en/nomenclature/KANGAL-SHEPHERD-DOG-331.html', 'http://www.fci.be/Nomenclature/Illustrations/331g02.jpg', 'http://www.fci.be/Nomenclature/Standards/331g02-en.pdf\r', NULL),
	(332, 'Czechoslovakian Wolfdog', 'Sheepdogs', 'Slovakia', 'http://www.fci.be/en/nomenclature/CZECHOSLOVAKIAN-WOLFDOG-332.html', '', 'http://www.fci.be/Nomenclature/Standards/332g01-en.pdf\r', NULL),
	(333, 'Polish Greyhound', 'Short-haired Sighthounds', 'Poland', 'http://www.fci.be/en/nomenclature/POLISH-GREYHOUND-333.html', '', 'http://www.fci.be/Nomenclature/Standards/333g10-en.pdf\r', NULL),
	(334, 'Korea Jindo Dog', 'Asian Spitz and related breeds', 'Republic Of Korea', 'http://www.fci.be/en/nomenclature/KOREA-JINDO-DOG-334.html', 'http://www.fci.be/Nomenclature/Illustrations/334g05.jpg', 'http://www.fci.be/Nomenclature/Standards/334g05-en.pdf\r', NULL),
	(335, 'Central Asia Shepherd Dog', 'Molossian type', 'Russia', 'http://www.fci.be/en/nomenclature/CENTRAL-ASIA-SHEPHERD-DOG-335.html', 'http://www.fci.be/Nomenclature/Illustrations/335g02-1.jpg', 'http://www.fci.be/Nomenclature/Standards/335g02-en.pdf\r', NULL),
	(336, 'Spanish Water Dog', 'Water Dogs', 'Spain', 'http://www.fci.be/en/nomenclature/SPANISH-WATER-DOG-336.html', '', 'http://www.fci.be/Nomenclature/Standards/336g08-en.pdf\r', NULL),
	(337, 'Italian Short-haired Segugio', 'Scent hounds', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-SHORT-HAIRED-SEGUGIO-337.html', 'http://www.fci.be/Nomenclature/Illustrations/337g06.jpg', 'http://www.fci.be/Nomenclature/Standards/337g06-en.pdf\r', NULL),
	(338, 'Thai Ridgeback Dog', 'Primitive type - Hunting Dogs', 'Thailand', 'http://www.fci.be/en/nomenclature/THAI-RIDGEBACK-DOG-338.html', '', 'http://www.fci.be/Nomenclature/Standards/338g05-en.pdf\r', NULL),
	(339, 'Parson Russell Terrier', 'Large and medium sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/PARSON-RUSSELL-TERRIER-339.html', 'http://www.fci.be/Nomenclature/Illustrations/339g03.jpg', 'http://www.fci.be/Nomenclature/Standards/339g03-en.pdf\r', NULL),
	(340, 'Saint Miguel Cattle Dog', 'Molossian type', 'Portugal', 'http://www.fci.be/en/nomenclature/SAINT-MIGUEL-CATTLE-DOG-340.html', '', 'http://www.fci.be/Nomenclature/Standards/340g02-en.pdf\r', NULL),
	(341, 'Brazilian Terrier', 'Large and medium sized Terriers', 'Brazil', 'http://www.fci.be/en/nomenclature/BRAZILIAN-TERRIER-341.html', 'http://www.fci.be/Nomenclature/Illustrations/341g03.jpg', 'http://www.fci.be/Nomenclature/Standards/341g03-en.pdf\r', NULL),
	(342, 'Australian Shepherd', 'Sheepdogs', 'United States Of America', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-SHEPHERD-342.html', '', 'http://www.fci.be/Nomenclature/Standards/342g01-en.pdf\r', NULL),
	(343, 'Italian Cane Corso', 'Molossian type', 'Italy', 'http://www.fci.be/en/nomenclature/ITALIAN-CANE-CORSO-343.html', 'http://www.fci.be/Nomenclature/Illustrations/343g02.jpg', 'http://www.fci.be/Nomenclature/Standards/343g02-en.pdf\r', NULL),
	(344, 'American Akita', 'Asian Spitz and related breeds', 'Japan', 'http://www.fci.be/en/nomenclature/AMERICAN-AKITA-344.html', '', 'http://www.fci.be/Nomenclature/Standards/344g05-en.pdf\r', NULL),
	(345, 'Jack Russell Terrier', 'Small sized Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/JACK-RUSSELL-TERRIER-345.html', 'http://www.fci.be/Nomenclature/Illustrations/345g03-2.jpg', 'http://www.fci.be/Nomenclature/Standards/345g03-en.pdf\r', NULL),
	(346, 'Dogo Canario', 'Molossian type', 'Spain', 'http://www.fci.be/en/nomenclature/DOGO-CANARIO-346.html', '', 'http://www.fci.be/Nomenclature/Standards/346g02-en.pdf\r', NULL),
	(347, 'White Swiss Shepherd Dog', 'Sheepdogs', 'Switzerland', 'http://www.fci.be/en/nomenclature/WHITE-SWISS-SHEPHERD-DOG-347.html', 'http://www.fci.be/Nomenclature/Illustrations/347g01-1.jpg', 'http://www.fci.be/Nomenclature/Standards/347g01-en.pdf\r', NULL),
	(348, 'Taiwan Dog', 'Primitive type - Hunting Dogs', 'Taiwan', 'http://www.fci.be/en/nomenclature/TAIWAN-DOG-348.html', '', 'http://www.fci.be/Nomenclature/Standards/348g05-en.pdf\r', NULL),
	(349, 'Romanian Mioritic Shepherd Dog', 'Sheepdogs', 'Romania', 'http://www.fci.be/en/nomenclature/ROMANIAN-MIORITIC-SHEPHERD-DOG-349.html', '', 'http://www.fci.be/Nomenclature/Standards/349g01-en.pdf\r', NULL),
	(350, 'Romanian Carpathian Shepherd Dog', 'Sheepdogs', 'Romania', 'http://www.fci.be/en/nomenclature/ROMANIAN-CARPATHIAN-SHEPHERD-DOG-350.html', 'http://www.fci.be/Nomenclature/Illustrations/350g01.jpg', 'http://www.fci.be/Nomenclature/Standards/350g01-en.pdf\r', NULL),
	(351, 'Australian Stumpy Tail Cattle Dog', 'Cattledogs (except Swiss Cattledogs)', 'Australia', 'http://www.fci.be/en/nomenclature/AUSTRALIAN-STUMPY-TAIL-CATTLE-DOG-351.html', '', 'http://www.fci.be/Nomenclature/Standards/351g01-en.pdf\r', NULL),
	(352, 'Russian Toy', 'Continental Toy Spaniel and Russian Toy', 'Russia', 'http://www.fci.be/en/nomenclature/RUSSIAN-TOY-352.html', 'http://www.fci.be/Nomenclature/Illustrations/352g09-1.jpg', 'http://www.fci.be/Nomenclature/Standards/352g09-en.pdf\r', NULL),
	(353, 'Cimarrón Uruguayo', 'Molossian type', 'Uruguay', 'http://www.fci.be/en/nomenclature/CIMARRON-URUGUAYO-353.html', '', 'http://www.fci.be/Nomenclature/Standards/353g02-en.pdf\r', NULL),
	(354, 'Polish Hunting Dog', 'Scent hounds', 'Poland', 'http://www.fci.be/en/nomenclature/POLISH-HUNTING-DOG-354.html', '', 'http://www.fci.be/Nomenclature/Standards/354g06-en.pdf\r', NULL),
	(355, 'Bosnian And Herzegovinian - Croatian Shepherd Dog', 'Molossian type', 'Bosnia And Herzegovina, Croatia', 'http://www.fci.be/en/nomenclature/BOSNIAN-AND-HERZEGOVINIAN-CROATIAN-SHEPHERD-DOG-355.html', '', 'http://www.fci.be/Nomenclature/Standards/355g02-en.pdf\r', NULL),
	(356, 'Danish-swedish Farmdog', 'Pinscher and Schnauzer type', 'Denmark, Sweden', 'http://www.fci.be/en/nomenclature/DANISH-SWEDISH-FARMDOG-356.html', 'http://www.fci.be/Nomenclature/Illustrations/356g02.jpg', 'http://www.fci.be/Nomenclature/Standards/356g02-en.pdf\r', NULL),
	(357, 'Romanian Bucovina Shepherd', 'Molossian type', 'Romania', 'http://www.fci.be/en/nomenclature/ROMANIAN-BUCOVINA-SHEPHERD-357.html', 'http://www.fci.be/Nomenclature/Illustrations/357g02.jpg', 'http://www.fci.be/Nomenclature/Standards/357g02-en.pdf\r', NULL),
	(358, 'Thai Bangkaew Dog', 'Asian Spitz and related breeds', 'Thailand', 'http://www.fci.be/en/nomenclature/THAI-BANGKAEW-DOG-358.html', 'http://www.fci.be/Nomenclature/Illustrations/358g05.jpg', 'http://www.fci.be/Nomenclature/Standards/358g05-en.pdf\r', NULL),
	(359, 'Miniature Bull Terrier', 'Bull type Terriers', 'Great Britain', 'http://www.fci.be/en/nomenclature/MINIATURE-BULL-TERRIER-359.html', 'http://www.fci.be/Nomenclature/Illustrations/359g03.jpg', 'http://www.fci.be/Nomenclature/Standards/359g03-en.pdf\r', NULL),
	(360, 'Lancashire Heeler', 'Sheepdogs', 'England', 'http://www.fci.be/en/nomenclature/LANCASHIRE-HEELER-360.html', 'http://www.fci.be/Nomenclature/Illustrations/360g01.jpg', 'http://www.fci.be/Nomenclature/Standards/360g01-en.pdf\r', NULL),
	(361, 'Segugio Maremmano', 'Scent hounds', 'Italy', 'http://www.fci.be/en/nomenclature/SEGUGIO-MAREMMANO-361.html', 'http://www.fci.be/Nomenclature/Illustrations/361g06-1.jpg', 'http://www.fci.be/Nomenclature/Standards/361g06-en.pdf', NULL);
/*!40000 ALTER TABLE `breed_type` ENABLE KEYS */;

-- Dumping structure for table animals.company
DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `established` date DEFAULT NULL,
  `employees` int(11) DEFAULT NULL,
  `revenue` decimal(12,2) DEFAULT NULL,
  `net_income` decimal(12,2) DEFAULT NULL,
  `securities` int(11) DEFAULT NULL,
  `security_price` decimal(10,2) DEFAULT NULL,
  `dividends` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.company: ~12 rows (approximately)
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` (`id`, `name`, `logo`, `established`, `employees`, `revenue`, `net_income`, `securities`, `security_price`, `dividends`) VALUES
	(1, 'Arco Vara', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC', '1994-07-04', 20, 3640000.00, -540000.00, 8998367, 1.09, 0.01),
	(2, 'Baltika', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT', '1997-05-09', 946, 44690000.00, -5120000.00, 4079485, 0.32, NULL),
	(3, 'Ekspress Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=EEG', '1995-06-21', 1698, 60490000.00, 10000.00, 29796841, 0.84, NULL),
	(4, 'Harju Elekter', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=HAE', '1996-05-14', 744, 120800000.00, 1550000.00, 17739880, 4.25, 0.18),
	(5, 'LHV Group', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV', '2005-01-25', 366, 64540000.00, 25240000.00, 26016485, 11.75, 0.21),
	(6, 'Merko Ehitus', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK', '1990-11-05', 740, 418010000.00, 19340000.00, 17700000, 9.30, 1.00),
	(7, 'Nordecon', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=NCN', '1998-01-01', 662, 223500000.00, 3380000.00, 32375483, 1.04, 0.12),
	(8, 'Pro Kapital Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=PKG', '1994-01-01', 91, 27990000.00, 16830000.00, 56687954, 1.43, NULL),
	(9, 'Tallink Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TAL', '1997-08-21', 7201, 949720000.00, 40050000.00, 669882040, 0.96, 0.12),
	(10, 'Tallinna Kaubamaja Grupp', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TKM', '1960-07-21', 4293, 681180000.00, 30440000.00, 40729200, 8.36, 0.71),
	(11, 'Tallinna Sadam', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TSM', '1991-12-25', 468, 130640000.00, 24420000.00, 263000000, 1.96, 0.13),
	(12, 'Tallinna Vesi', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=TVE', '1967-01-01', 314, 62780000.00, 24150000.00, 20000000, 10.80, 0.75);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;

-- Dumping structure for table animals.pet
DROP TABLE IF EXISTS `pet`;
CREATE TABLE IF NOT EXISTS `pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pet_type` int(11) DEFAULT NULL,
  `breed_id` int(11) DEFAULT NULL,
  `breedSize` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inHeat` int(1) DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `personality` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(141) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`pet_type`),
  KEY `FK2_owner_id` (`ownerId`),
  KEY `FK_breed_id` (`breed_id`),
  CONSTRAINT `FK2_owner_id` FOREIGN KEY (`ownerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_breed_id` FOREIGN KEY (`breed_id`) REFERENCES `breed_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.pet: ~6 rows (approximately)
/*!40000 ALTER TABLE `pet` DISABLE KEYS */;
INSERT INTO `pet` (`id`, `name`, `age`, `gender`, `pet_type`, `breed_id`, `breedSize`, `inHeat`, `ownerId`, `personality`, `picture`, `description`, `certification`) VALUES
	(1, 'Willie', 3, 'boy', 1, 297, 'Large', 1, 2, 'confident', 'http://localhost:8101/files/file/1fa69cba-fc48-4d72-a9a1-520418b07631.jpg', 'Great companion to have. Always up for a hike. Likes to snuggle a lot.', 'Winner of BEST MOUNTAIN DOG 2018 award.'),
	(2, 'Pasha', 1, 'boy', 1, 41, 'Large', 0, 3, 'laidback', 'http://localhost:8101/files/file/c4608d72-4b77-45ad-b52d-54daa0152d9a.jpg', 'гав-гав-гав-гав! ***also winks***', '1st dog of Mother Russia'),
	(3, 'Jenny', 4, 'girl', 1, 5, 'Small', 1, 4, 'adaptable', 'http://localhost:8101/files/file/508c3bad-a5e7-41bd-b317-ac80aceb9052.jpg', 'I love chasing round things.', 'Beauty queen'),
	(4, 'Charlie', 3, 'boy', 1, 97, 'Small', 0, 5, 'shy', 'http://localhost:8101/files/file/d3fffa18-edf0-4a44-a5e1-81ddb44d839d.jpg', 'I love my owner.', 'Ladies guy'),
	(5, 'Snoopy', 2, 'boy', 1, 184, 'Medium', 0, 6, 'independent', 'http://localhost:8101/files/file/a88f9806-7a03-4c13-b472-17bc510ca9ef.jpg', 'That\'s how we do it in the black community; we give back to the people who made us who we are. We never forget that.', '#1 gangsta dog'),
	(6, 'Boo', 5, 'girl', 1, 30, 'Tiny', 1, 7, 'confident', 'http://localhost:8101/files/file/a2a99eff-743c-4500-b419-589f90979e44.jpg', 'I\'m famous, you\'ll probably seen me on FB', 'First FB dog star'),
	(7, 'Carl', 4, 'boy', 1, 2, 'Large', 0, 8, 'independent', 'http://localhost:8101/files/file/510cbbca-07aa-4971-b565-9bf7e313a2e0.jpg', 'I love running', 'Best runner ever'),
	(8, 'Jacky', 3, 'boy', 1, 147, 'Small', 0, 9, 'confident', 'http://localhost:8101/files/file/3eace737-184e-4f85-824a-1e42e7c48c89.jpg', 'Jacky is very energetic.', 'Best of everything'),
	(9, 'Bo', 4, 'boy', 1, 37, 'Small', 0, 10, 'shy', 'http://localhost:8101/files/file/d8706e27-fc36-4ae7-8d43-233adc2c72b0.jpg', 'Classy dog. Well-trained. Behaves well, too.', '#1 dog of U S and A'),
	(10, 'Herman', 5, 'boy', 1, 149, 'Small', 1, 11, 'shy', 'http://localhost:8101/files/file/50127c10-f595-4503-b9e6-79a8476e80c3.jpg', 'Sometimes Michael leaves his food on the table and after eating it I feel funny.', '#1 dog of SWIMMERS'),
	(11, 'Dessie', 1, 'girl', 1, 76, 'Medium', 0, 12, 'confident', 'http://localhost:8101/files/file/5edf6d47-35bc-4433-a211-ef834b18e126.jpg', 'Olen alles väike', 'Kõige nunnum koer ever');
/*!40000 ALTER TABLE `pet` ENABLE KEYS */;

-- Dumping structure for table animals.pet_type
DROP TABLE IF EXISTS `pet_type`;
CREATE TABLE IF NOT EXISTS `pet_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.pet_type: ~3 rows (approximately)
/*!40000 ALTER TABLE `pet_type` DISABLE KEYS */;
INSERT INTO `pet_type` (`id`, `name`) VALUES
	(1, 'dog'),
	(2, 'cat'),
	(3, 'rabbit');
/*!40000 ALTER TABLE `pet_type` ENABLE KEYS */;

-- Dumping structure for function animals.proper_case
DROP FUNCTION IF EXISTS `proper_case`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `proper_case`(str varchar(128)) RETURNS varchar(128) CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci
BEGIN
DECLARE n, pos INT DEFAULT 1;
DECLARE sub, proper VARCHAR(128) DEFAULT '';

if length(trim(str)) > 0 then
    WHILE pos > 0 DO
        set pos = locate(' ',trim(str),n);
        if pos = 0 then
            set sub = lower(trim(substr(trim(str),n)));
        else
            set sub = lower(trim(substr(trim(str),n,pos-n)));
        end if;

        set proper = concat_ws(' ', proper, concat(upper(left(sub,1)),substr(sub,2)));
        set n = pos + 1;
    END WHILE;
end if;

RETURN trim(proper);
END//
DELIMITER ;

-- Dumping structure for table animals.swipe
DROP TABLE IF EXISTS `swipe`;
CREATE TABLE IF NOT EXISTS `swipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `dislike` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_from_pet_id1` (`from`),
  KEY `FK_like_pet_id2` (`like`),
  KEY `FK_dislike_pet_id3` (`dislike`),
  CONSTRAINT `FK_pet_id1` FOREIGN KEY (`from`) REFERENCES `pet` (`id`),
  CONSTRAINT `FK_pet_id2` FOREIGN KEY (`like`) REFERENCES `pet` (`id`),
  CONSTRAINT `FK_pet_id3` FOREIGN KEY (`dislike`) REFERENCES `pet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.swipe: ~9 rows (approximately)
/*!40000 ALTER TABLE `swipe` DISABLE KEYS */;
INSERT INTO `swipe` (`id`, `from`, `like`, `dislike`) VALUES
	(5, 6, 4, NULL),
	(6, 6, NULL, 1),
	(7, 6, 5, NULL),
	(8, 6, 2, NULL),
	(9, 6, 3, NULL),
	(10, 11, 8, NULL),
	(11, 11, 6, NULL),
	(12, 11, NULL, 9),
	(13, 11, 2, NULL),
	(14, 11, 7, NULL),
	(15, 11, 5, NULL),
	(16, 11, NULL, 4),
	(17, 11, NULL, 1),
	(18, 11, 3, NULL),
	(19, 11, 10, NULL);
/*!40000 ALTER TABLE `swipe` ENABLE KEYS */;

-- Dumping structure for table animals.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` blob DEFAULT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(141) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 3` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table animals.user: ~7 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `name`, `age`, `gender`, `picture`, `location`, `description`, `username`, `password`) VALUES
	(1, NULL, NULL, NULL, NULL, NULL, NULL, 'admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei'),
	(2, 'Richard Gere', 70, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F63626637373333612D363461332D346631382D626531662D3165313237613132373235322E6A7067, 'City Centre', 'Award-winning (most likely) actor looking to have some fun. Hit me up!', 'RichardGere', '$2a$10$roDHXGftyfBW.5zplLlWJ.VRH3ABHBlhBhui39J/M9ADQYAAK/w52'),
	(3, 'Vladimir Putin', 67, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F37363162333366382D353635302D343631302D386433612D6632393131306633373361372E6A7067, 'Lasnamäe', '140% of the people of Russia like me. Is nice ***winks***', 'VladimirPutin', '$2a$10$pnzUJ4YYByPwVC75vQRkkufU2kKH0vQMg8jHYwLByhv1UU9s/cqia'),
	(4, 'Jennifer', 25, 'Female', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F61613461613538362D643931312D343061632D383933352D6330646132383964396561362E6A7067, 'Mustamäe', 'I\'m an artist!', 'Velvet', '$2a$10$eyGwpsnqUzNO12GzUdOfiO.OULO1Wn8kwhimzWMzeZrJ8wXktoSdm'),
	(5, 'Marek Sadam', 40, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F33353564333362362D316231652D346662312D383534632D6437316165346436353665342E6A7067, 'Haabersti', 'N-EURO', 'Sadamasild', '$2a$10$5IP5A0tCADqCKL8FlI5Fd.6HRV7wUghdZ1p5oJqrl.ErPOJTxJVB6'),
	(6, 'Snoop Dogg', 48, 'M', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F38633938633464332D336431612D343835612D383764392D3433306661616637646236642E6A7067, 'City Centre', 'When I\'m not longer rapping, I want to open up an ice cream parlor and call myself Scoop Dogg.', 'SnoopDogg', 'SnoopDogg'),
	(7, 'Irene Ahn', 30, 'Female', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F33313139616535312D623430392D346665322D613930302D3131333730313931366632392E6A706567, 'Northern Tallinn', 'I\'m athlete, love running in the mountains.', 'Boo', '$2a$10$Fb9FW5/3sZixBnuHPp64o.LhQ9EvHiJxTxaEaIEaeYlFdZExPJw3S'),
	(8, 'Maarja', 25, 'Female', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F31626637353765652D333435622D343131632D396330312D3064356561343161346430642E6A7067, 'Haabersti', 'Meeldib talv', 'Lillelaps', '$2a$10$NCY6t11CD1Uw2Wb5tzfBrO/QIpG2KS32FaWOEV6pZVM8vbjeqtEX2'),
	(9, 'Tõnu', 59, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F38373865346465312D363535372D343632612D616663312D6333626231383735623362382E6A7067, 'City Centre', 'I really into TRX, anyone interested, let\'s go together.', 'TRX', '$2a$10$OvtYxask0Q4SL3vPInuuJeKN.XGSK0d/4nFhQFAVUoYzaM0ZksnE.'),
	(10, 'Barack Obama', 58, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F63663662303234342D613761372D343531302D393138342D3166333863613963386266382E6A7067, 'City Centre', '\'If I had to name my greatest strength, I guess it would be my humility. Greatest weakness, it\'s possible that I\'m a little too awesome.', 'BarackObama', '$2a$10$/t1xSUAz2grzve5oW90WH.ypT596FCVczPENYndnSx/jLGT988uI6'),
	(11, 'Michael Phelps', 34, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F65383939393037312D666632622D346339392D623963342D3266333938313233323435372E6A7067, 'Haabersti', 'I prefer gold. Although, I have been known to prefer green from time to time as well ;)', 'MichaelPhelps', '$2a$10$ryiffBUTshb91u/3IEtrTOltalqR5vx8FMdHP8aR.gvS5nu7a.NNm'),
	(12, 'Egert', 28, 'Male', _binary 0x687474703A2F2F6C6F63616C686F73743A383130312F66696C65732F66696C652F64656632343238662D326231622D343764382D396338312D3937326236353630626563352E6A7067, 'City Centre', 'Mul on nunnu koer', 'Egert', '$2a$10$DmVHlXybrs.k27Br8bRyX.OqMmPKEvGS4SSAVYted50ZfQDeO8RIO');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
