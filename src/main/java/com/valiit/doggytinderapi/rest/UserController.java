package com.valiit.doggytinderapi.rest;

import com.valiit.doggytinderapi.dto.GenericResponseDto;
import com.valiit.doggytinderapi.dto.JwtRequestDto;
import com.valiit.doggytinderapi.dto.JwtResponseDto;
import com.valiit.doggytinderapi.dto.UserRegistrationDto;
import com.valiit.doggytinderapi.model.User;
import com.valiit.doggytinderapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PutMapping("/register/update")
    public GenericResponseDto update(@RequestBody UserRegistrationDto userRegistration) {
        return userService.update(userRegistration);
    }

    @GetMapping("/{username}")
    public User getUser(@PathVariable("username") String username) {
        return userService.getUser(username);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }
}