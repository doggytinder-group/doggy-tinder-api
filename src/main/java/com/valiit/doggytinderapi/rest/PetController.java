package com.valiit.doggytinderapi.rest;

import com.valiit.doggytinderapi.dto.*;
import com.valiit.doggytinderapi.model.Breed;
import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.repository.PetRepository;
import com.valiit.doggytinderapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pets")
@CrossOrigin("*")
public class PetController {

    @Autowired
    private UserService userService;

    @Autowired
    private PetRepository petRepository;

    @PostMapping("/registerPet")
    public GenericResponseDto registerPet(@RequestBody PetRegistrationDto petRegistration) {
        return userService.registerPet(petRegistration);
    }

    @PutMapping("/registerPet/update")
    public GenericResponseDto updatePet(@RequestBody PetRegistrationDto petRegistration) {
        return userService.updatePet(petRegistration);
    }

    @GetMapping("/{username}")
    public Pet getPet(@PathVariable("username") String username) {
        return userService.getPet(username);
    }

    @GetMapping("/generateBreedTypeList")
    public List<Breed> getBreed() {
        return petRepository.getBreed();
    }

}

