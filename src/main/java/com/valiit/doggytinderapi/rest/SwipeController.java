package com.valiit.doggytinderapi.rest;

import com.valiit.doggytinderapi.dto.CompanyDto;
import com.valiit.doggytinderapi.dto.GenericResponseDto;
import com.valiit.doggytinderapi.dto.SwipeRegistrationDto;
import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.repository.SwipeRepository;
import com.valiit.doggytinderapi.service.SwipeService;
import com.valiit.doggytinderapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/swipes")
@CrossOrigin("*")
public class SwipeController {

    @Autowired
    private SwipeService swipeService;

    @Autowired
    private SwipeRepository swipeRepository;

    @Autowired
    private UserService userService;

    @PostMapping("/registerSwipe")
    public GenericResponseDto addSwipe(@RequestBody SwipeRegistrationDto swipeRegistration) {
        System.out.println(swipeRegistration);
        return swipeService.registerSwipe(swipeRegistration);
    }

    @GetMapping("/swiping")
    public Pet getPet() {
        return swipeService.getPet();
    }

    @GetMapping("/matches")
    public List<Pet> getMatches() {
        int currentPetId = userService.getLoggedInUserPet().getId();
        return swipeRepository.getMatches(currentPetId);
    }
}
