package com.valiit.doggytinderapi.dto;

public class PetRegistrationDto {

    private int id; //võta pet eest ära kõigiöl, muuda samaks mis visual studios get crediential...
    private String name;
    private int age;
    private String gender;
    private int petType;
    private int breedId;
    private String breedSize;
    private boolean inHeat;
    private String personality; //võtsin [] ära
    private String picture;
    private String description;
    private String certification; //võtsin [] ära
    private int ownerId;

    public PetRegistrationDto(int id, String name, int age, String gender, int petType, int breedId, String breedSize, boolean inHeat, String personality, String picture, String description, String certification, int ownerId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.petType = petType;
        this.breedId = breedId;
        this.breedSize = breedSize;
        this.inHeat = inHeat;
        this.personality = personality;
        this.picture = picture;
        this.description = description;
        this.certification = certification;
        this.ownerId = ownerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPetType() {
        return petType;
    }

    public void setPetType(int petType) {
        this.petType = petType;
    }

    public int getBreedId() {
        return breedId;
    }

    public void setBreedId(int breedId) {
        this.breedId = breedId;
    }

    public String getBreedSize() {
        return breedSize;
    }

    public void setBreedSize(String breedSize) {
        this.breedSize = breedSize;
    }

    public boolean isInHeat() {
        return inHeat;
    }

    public void setInHeat(boolean inHeat) {
        this.inHeat = inHeat;
    }

    public String getPersonality() {
        return personality;
    }

    public void setPersonality(String personality) {
        this.personality = personality;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public int setOwnerId(int ownerId) {
        return this.ownerId = ownerId;
    }
}
