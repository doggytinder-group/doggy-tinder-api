package com.valiit.doggytinderapi.dto;

public class SwipeRegistrationDto {

    int id;
    int from;
    Integer like;
    Integer dislike;

    public SwipeRegistrationDto() {
    }

    public SwipeRegistrationDto(int id, int from, Integer like, Integer dislike) {
        this.id = id;
        this.from = from;
        this.like = like;
        this.dislike = dislike;
    }

    public SwipeRegistrationDto(int from, Integer like, Integer dislike) {
        this.from = from;
        this.like = like;
        this.dislike = dislike;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public Integer getDislike() {
        return dislike;
    }

    public void setDislike(Integer dislike) {
        this.dislike = dislike;
    }
}
