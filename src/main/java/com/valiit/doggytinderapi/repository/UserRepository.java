package com.valiit.doggytinderapi.repository;

import com.valiit.doggytinderapi.model.UploadResponse;
import com.valiit.doggytinderapi.model.User;
import com.valiit.doggytinderapi.rest.FileController;
import com.valiit.doggytinderapi.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private FileController fileController;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }
///!!!!!
    public void addUser(User user) {
        jdbcTemplate.update("insert into `user` (`username`, `password`, `name`, `age`, `gender`, `picture`," +
                "`location`, `description`) values (?, ?, ?, ?, ?, ?, ?, ?)", user.getUsername(), user.getPassword(),
                user.getName(), user.getAge(), String.valueOf(user.getGender()), user.getPicture(), user.getLocation(), user.getDescription());
    }

    public void updateUser(User user) {
        jdbcTemplate.update(
                "update `user` set `name` = ?, `age` = ?, `gender` = ?, `picture` = ?, `location` = ?, `description` = ?, `password` = ? WHERE `username` = ?",
                user.getName(), user.getAge(), String.valueOf(user.getGender()), user.getPicture(), user.getLocation(), user.getDescription(), user.getPassword(), user.getUsername());
    }

    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"),
                        rs.getString("name"), rs.getInt("age"), rs.getString("gender"), rs.getString("picture"),
                        rs.getString("location"), rs.getString("description"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }


    public User getUserById(int id) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `id` = ?",
                new Object[]{id},
                (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("username"), rs.getString("password"),
                        rs.getString("name"), rs.getInt("age"), rs.getString("gender"), rs.getString("picture"),
                        rs.getString("location"), rs.getString("description"))
        );
        return users.size() > 0 ? users.get(0) : null;
    }
}
