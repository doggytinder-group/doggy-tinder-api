package com.valiit.doggytinderapi.repository;

import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.model.Swipe;
import com.valiit.doggytinderapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class SwipeRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private UserService userService;

    public void addSwipe(Swipe swipe) { jdbcTemplate.update("insert into `swipe` (`from`, `like`, `dislike`) values (?, ?, ?)",
                swipe.getFrom(), swipe.getLike(), swipe.getDislike());
    }

    public List<Pet> getMatches(int petId) {
        List<Pet> matches = jdbcTemplate.query("SELECT p.*, bt.name as breed_name FROM `pet` p INNER JOIN breed_type bt on bt.id = p.breed_id WHERE p.`id` IN (SELECT DISTINCT(`from`) FROM `swipe` WHERE `from` IN " +
                "(SELECT DISTINCT(`like`) FROM `swipe` WHERE `from` = ? AND `like` IS NOT NULL) AND `like` = ?)",
                new Object[]{petId, petId},
                (rs, rowNum) -> new Pet(rs.getInt("id"), rs.getString("name"), rs.getInt("age"),
                        rs.getString("gender"), rs.getInt("pet_type"), rs.getInt("breed_id"),
                        rs.getString("breedSize"), rs.getBoolean("inHeat"),
                        rs.getString("personality"), rs.getString("picture"), rs.getString("description"),
                        rs.getString("certification"), rs.getInt("ownerId"), rs.getString("breed_name"))
                );
        if (matches.size() > 0) {
            return matches;
        } else {
            return null;
        }
    }
// final comment vol2
    public Pet getPetById(int id) {
        List<Pet> pets = jdbcTemplate.query(
                "select p.*, bt.name as breed_name from `pet` p inner join breed_type bt on bt.id = p.breed_id where p.`id` = ?",
                new Object[]{id},
                (rs, rowNum) -> new Pet(rs.getInt("id"), rs.getString("name"), rs.getInt("age"),
                        rs.getString("gender"), rs.getInt("pet_type"), rs.getInt("breed_id"),
                        rs.getString("breedSize"), rs.getBoolean("inHeat"),
                        rs.getString("personality"), rs.getString("picture"), rs.getString("description"),
                        rs.getString("certification"), rs.getInt("ownerId"), rs.getString("breed_name"))
        );
        if (pets.size() > 0) {
            Pet pet = pets.get(0);
            pet.setOwner(userRepository.getUserById(pet.getOwnerId()));
            return pet;
        } else {
            return null;
        }
    }

    public List<Integer> getAlreadySwipedPets(int id) {
        List<Integer> swipedPets = jdbcTemplate.queryForList("SELECT DISTINCT `like` from `swipe` where `from` = ? AND `like` IS NOT NULL UNION SELECT DISTINCT `dislike` from `swipe` where `from` = ? AND `dislike` IS NOT NULL", new Object[] {id, id}, Integer.class);
        System.out.println("Swipitud on juba" + swipedPets);
        return swipedPets;
    }

    public boolean checkSwipingId(int id) {
        if (id == userService.getLoggedInUserPet().getId()) {
            return false;
        }
        else if (getAlreadySwipedPets(userService.getLoggedInUserPet().getId()).contains(id)){
            System.out.println("jah, sisaldab");
            return false;
        }
        else {
            System.out.println("Kõik on korras");
            return true;
        }

        // mitte kuvada hetke kasutajat
        // mitte kuvada kasutajat, keda on juba swiping listis likitud
    }



}
