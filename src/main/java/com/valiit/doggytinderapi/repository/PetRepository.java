package com.valiit.doggytinderapi.repository;

import com.valiit.doggytinderapi.model.Breed;
import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.model.User;
import com.valiit.doggytinderapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PetRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

//    @Autowired
//    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    public void addPet(Pet pet) {
        jdbcTemplate.update("insert into `pet` (`name`, `age`, `gender`, `pet_type`, `breed_id`," +
                        "`breedSize`, `inHeat`, `personality`, `picture`, `description`, `certification`, `ownerId`) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", pet.getName(), pet.getAge(), String.valueOf(pet.getGender()), pet.getPetType(),
                pet.getBreedId(), pet.getBreedSize(), pet.isInHeat(), pet.getPersonality(), pet.getPicture(),
                pet.getDescription(), pet.getCertification(), pet.getOwnerId());
    }

    public void updatePet(Pet pet) {
        jdbcTemplate.update(
                "update `pet` set `age` = ?, `gender` = ?, `pet_type` = ?, `breed_id` = ?," +
                        " `breedSize` = ?, `inHeat` = ?, `personality` = ?, `picture` = ?, `description` = ?, `certification` = ?, WHERE `name` = ?",
                pet.getAge(), String.valueOf(pet.getGender()), pet.getPetType(),
                pet.getBreedId(), pet.getBreedSize(), pet.isInHeat(), pet.getPersonality(), pet.getPicture(),
                pet.getDescription(), pet.getCertification(), pet.getName());
    }

    public List<Breed> getBreed() {
        return jdbcTemplate.query("select `id`, `name` from `breed_type`", mapBreedRows);
    }

    private RowMapper<Breed> mapBreedRows = (rs, rowNum) -> {
        Breed breed = new Breed();
        breed.setId(rs.getInt("id"));
        breed.setName(rs.getString("name"));
        return breed;
    };

    //uus comment
    //et saada pet change view
    public Pet getPetByUsername(String username) {
        List<Pet> pets = jdbcTemplate.query(
                "select p.*, bt.name as breed_name from `pet` p inner join breed_type bt on bt.id = p.breed_id where p.`ownerId` in (select u.id from user u where u.username = ?)",
                new Object[]{username},
                (rs, rowNum) -> new Pet(rs.getInt("id"), rs.getString("name"), rs.getInt("age"),
                        rs.getString("gender"), rs.getInt("pet_type"), rs.getInt("breed_id"),
                        rs.getString("breedSize"), rs.getBoolean("inHeat"),
                        rs.getString("personality"), rs.getString("picture"), rs.getString("description"),
                        rs.getString("certification"), rs.getInt("ownerId"), rs.getString("breed_name"))
        );
        return pets.size() > 0 ? pets.get(0) : null;
    }

    public int countPets() {
        return jdbcTemplate.queryForObject("select count(id) from `pet`", Integer.class);
    }
}

