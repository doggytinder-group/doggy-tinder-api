package com.valiit.doggytinderapi;

import com.valiit.doggytinderapi.repository.PetRepository;
import com.valiit.doggytinderapi.repository.SwipeRepository;
import org.apache.catalina.startup.ClassLoaderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@SpringBootApplication
public class DoggyTinderApiApplication {

	public static void main(String[] args) {

		SpringApplication.run(DoggyTinderApiApplication.class, args);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
