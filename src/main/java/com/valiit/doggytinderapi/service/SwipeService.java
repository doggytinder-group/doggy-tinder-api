package com.valiit.doggytinderapi.service;


import com.valiit.doggytinderapi.dto.GenericResponseDto;
import com.valiit.doggytinderapi.dto.SwipeRegistrationDto;
import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.model.Swipe;
import com.valiit.doggytinderapi.repository.PetRepository;
import com.valiit.doggytinderapi.repository.SwipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.tags.form.InputTag;

import java.util.List;

@Service
public class SwipeService {

    @Autowired
    private SwipeRepository swipeRepository;

    @Autowired
    private PetRepository petRepository;

    @Autowired
    private UserService userService;

    public GenericResponseDto registerSwipe(SwipeRegistrationDto swipeRegistration) {
        GenericResponseDto swipeResponseDto = new GenericResponseDto();
        Swipe swipe = new Swipe(swipeRegistration.getFrom(), swipeRegistration.getLike(), swipeRegistration.getDislike());
        swipeRepository.addSwipe(swipe);

        return swipeResponseDto;
    }

    public Pet getPet() {
        Pet pet = swipeRepository.getPetById(generateRandomNr());
        if (swipeRepository.getAlreadySwipedPets(userService.getLoggedInUserPet().getId()).size() >= (petRepository.countPets() - 1)) {
            System.out.println("Loomad said otsa!");
            return null;
        } else {
            while (pet == null || swipeRepository.checkSwipingId(pet.getId()) == false) {
                pet = swipeRepository.getPetById(generateRandomNr());
            }
            return pet;
        }
    }

    public int generateRandomNr() {
        return ((int) (Math.random() * petRepository.countPets() + 1));
    }
}

