package com.valiit.doggytinderapi.service;

import com.valiit.doggytinderapi.dto.*;
import com.valiit.doggytinderapi.model.Pet;
import com.valiit.doggytinderapi.model.User;
import com.valiit.doggytinderapi.repository.PetRepository;
import com.valiit.doggytinderapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PetRepository petRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public GenericResponseDto register(UserRegistrationDto userRegistration) {
            GenericResponseDto responseDto = new GenericResponseDto();
            User user = new User(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()),
                    userRegistration.getName(), userRegistration.getAge(), userRegistration.getGender(), userRegistration.getPicture(),
                    userRegistration.getLocation(), userRegistration.getDescription());
            if (!userRepository.userExists(userRegistration.getUsername())) {
                userRepository.addUser(user);
            } else {
                responseDto.getErrors().add("User with the specified username already exists.");
            }
            return responseDto;
    }

    public GenericResponseDto update(UserRegistrationDto userRegistration) {
        GenericResponseDto responseDto = new GenericResponseDto();
        User user = new User(userRegistration.getUsername(), passwordEncoder.encode(userRegistration.getPassword()),
                userRegistration.getName(), userRegistration.getAge(), userRegistration.getGender(), userRegistration.getPicture(),
                userRegistration.getLocation(), userRegistration.getDescription());
        System.out.println(user);
        userRepository.updateUser(user);
        return responseDto;
    }

    public GenericResponseDto registerPet(PetRegistrationDto petRegistration) {
        int ownerId = getLoggedInUser().getId();

        GenericResponseDto petResponseDto = new GenericResponseDto();
        Pet pet = new Pet(petRegistration.getName(), petRegistration.getAge(), petRegistration.getGender(), petRegistration.getPetType(),
                petRegistration.getBreedId(), petRegistration.getBreedSize(), petRegistration.isInHeat(), petRegistration.getPersonality(), petRegistration.getPicture(),
                petRegistration.getDescription(), petRegistration.getCertification(), petRegistration.setOwnerId(ownerId));
        petRepository.addPet(pet);
        return petResponseDto;
    }

    public GenericResponseDto updatePet(PetRegistrationDto petRegistration) {
        int ownerId = getLoggedInUser().getId();

        GenericResponseDto petResponseDto = new GenericResponseDto();
        Pet pet = new Pet(petRegistration.getName(), petRegistration.getAge(), petRegistration.getGender(), petRegistration.getPetType(),
                petRegistration.getBreedId(), petRegistration.getBreedSize(), petRegistration.isInHeat(), petRegistration.getPersonality(), petRegistration.getPicture(),
                petRegistration.getDescription(), petRegistration.getCertification(), petRegistration.setOwnerId(ownerId));
        petRepository.updatePet(pet);
        return petResponseDto;
    }

    public User getLoggedInUser() {
        String loggedInUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        User loggedInUser = getUser(loggedInUsername);
        return loggedInUser;
    }

    public Pet getLoggedInUserPet() {
        return petRepository.getPetByUsername(getLoggedInUser().getUsername());
    }
    
    public JwtResponseDto authenticate(JwtRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword());
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new JwtResponseDto(userDetails.getId(), userDetails.getUsername(), token);
    }

    public User getUser(String username) {
//        Assert.isTrue(username > 0, "Check if username exists.");
        User user = userRepository.getUserByUsername(username);
        return user;
//        return Transformer.toPetRegistrationDto(username);
    }


    public Pet getPet(String username) {
        Pet pet = petRepository.getPetByUsername(username);
        return pet;
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}