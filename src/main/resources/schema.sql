DROP TABLE IF EXISTS `swipe`;
DROP TABLE IF EXISTS `pet`;
DROP TABLE IF EXISTS `breed_type`;
DROP TABLE IF EXISTS `pet_type`;
DROP TABLE IF EXISTS `user`;


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` blob DEFAULT NULL,
  `location` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(141) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 3` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



CREATE TABLE IF NOT EXISTS `pet_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `breed_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pet_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pet_type_id` (`pet_type_id`),
  CONSTRAINT `FK_pet_type_id` FOREIGN KEY (`pet_type_id`) REFERENCES `pet_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=362 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `gender` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pet_type` int(11) DEFAULT NULL,
  `breed_id` int(11) DEFAULT NULL,
  `breedSize` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inHeat` int(1) DEFAULT NULL,
  `ownerId` int(11) DEFAULT NULL,
  `personality` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(500) DEFAULT NULL,
  `description` varchar(141) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`pet_type`),
  KEY `FK2_owner_id` (`ownerId`),
  KEY `FK_breed_id` (`breed_id`),
  CONSTRAINT `FK2_owner_id` FOREIGN KEY (`ownerId`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_breed_id` FOREIGN KEY (`breed_id`) REFERENCES `breed_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `swipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) DEFAULT NULL,
  `like` int(11) DEFAULT NULL,
  `dislike` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_from_pet_id1` (`from`),
  KEY `FK_like_pet_id2` (`like`),
  KEY `FK_dislike_pet_id3` (`dislike`),
  CONSTRAINT `FK_pet_id1` FOREIGN KEY (`from`) REFERENCES `pet` (`id`),
  CONSTRAINT `FK_pet_id2` FOREIGN KEY (`like`) REFERENCES `pet` (`id`),
  CONSTRAINT `FK_pet_id3` FOREIGN KEY (`dislike`) REFERENCES `pet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



























DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `logo` VARCHAR(255) NULL,
  `established` DATE NULL,
  `employees` INT NULL,
  `revenue` DECIMAL(12,2) NULL,
  `net_income` DECIMAL(12,2) NULL,
  `securities` INT NULL,
  `security_price` DECIMAL(10,2) NULL,
  `dividends` DECIMAL(10,2) NULL,
  PRIMARY KEY (id)
);